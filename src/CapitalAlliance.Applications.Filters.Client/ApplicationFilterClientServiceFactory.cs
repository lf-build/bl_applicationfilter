﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;

using LendFoundry.Foundation.Logging;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else

using Microsoft.Framework.DependencyInjection;

#endif

namespace CapitalAlliance.Applications.Filters.Client
{
    public class ApplicationFilterClientServiceFactory : IApplicationFilterClientServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public ApplicationFilterClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ApplicationFilterClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }

        private Uri Uri { get; }

        public IApplicationFilterService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("application_filters");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicationFilterClientService(client);
        }
    }
}