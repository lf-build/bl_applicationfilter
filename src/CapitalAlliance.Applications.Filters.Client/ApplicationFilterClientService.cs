﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Linq;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else

using Microsoft.AspNet.Mvc;

#endif

using LendFoundry.Foundation.Client;

namespace CapitalAlliance.Applications.Filters.Client
{
    public class ApplicationFilterClientService : IApplicationFilterService
    {
        public ApplicationFilterClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/all", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public async Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/month", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);
        }

        public async Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/year", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(name))
                throw new InvalidArgumentException($"{nameof(name)} is mandatory", nameof(name));

            var request = new RestRequest("/{sourceType}/{sourceId}/search/{name}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(name), name);

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);

            return await Client.ExecuteAsync<int>(request);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/total-approved-amount", Method.GET);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.Execute<double>(request);
            //if (result == null)
            //    return 0;

            //return Convert.ToDouble(result);
        }

        public async Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var request = new RestRequest("/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return await Client.ExecuteAsync<FilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var request = new RestRequest("/expired/all", Method.GET);
            var strBuilder = new StringBuilder();
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(List<string> excludedStatuses)
        {
            var request = new RestRequest("/expired/all/excludedStatuses", Method.POST);
            request.AddJsonBody(excludedStatuses);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        // public async Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityIds)
        // {
        //     var request = new RestRequest("/getapplicationsbyentityids/all", Method.POST);
        //     request.AddJsonBody(entityIds);
        //     return await Client.ExecuteAsync<List<FilterView>>(request);
        // }

        private void Validade(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public async Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterType, string parameterValue)
        {
            var request = new RestRequest("duplicateexists/{parametertype}/{parametervalue}", Method.GET);
            request.AddUrlSegment("parametertype", parameterType.ToString());
            request.AddUrlSegment("parametervalue", parameterValue);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber, string productId = null)
        {
            var request = new RestRequest("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}/{productId?}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            if (!string.IsNullOrEmpty(productId))
                request.AddUrlSegment(nameof(productId), productId);
            return await Client.ExecuteAsync<IFilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityIds)
        {
            var request = new RestRequest("/getapplicationsbyentityids/all", Method.POST);
            request.AddJsonBody(entityIds);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        #region taging related method

        public async Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        {
            var request = new RestRequest("tag", Method.GET);
            AppendItems(request, tags.ToList(), "tags");
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            var request = new RestRequest("/count/tag", Method.GET);
            AppendItems(request, tags.ToList(), "tags");
            return await Client.ExecuteAsync<int>(request);
        }

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            var request = new RestRequest("tag/taginfo/{tag}", Method.GET);
            request.AddUrlSegment("tag", tag);

            return await Client.ExecuteAsync<IEnumerable<FilterViewTags>>(request);
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("applytags/{applicationnumber}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            AppendItems(request, tags.ToList(), "tags");
            Client.Execute(request);
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("removetags/{applicationnumber}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            AppendItems(request, tags.ToList(), "tags");
            Client.Execute(request);
        }

        #endregion taging related method

        private static void AppendItems(IRestRequest request, IReadOnlyList<string> items, string itemType)
        {
            if (items == null || !items.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < items.Count; index++)
            {
                var itemValue = items[index];
                url = url + $"/{{{itemType}{index}}}";
                request.AddUrlSegment($"{itemType}{index}", itemValue);
            }
            request.Resource = url;
        }

        public IFilterViewTags GetApplicationTagInformation(string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IFilterView>> GetAllUnassigned()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IFilterView>> GetAllUnassignedByUser()
        {
            throw new NotImplementedException();
        }

        public FileContentResult ExportAll(string templateName)
        {
            throw new NotImplementedException();
        }

        public Task<FileContentResult> ExportAllByStatus(string templateName, IEnumerable<string> statuses)
        {
            throw new NotImplementedException();
        }

        public Task<FileContentResult> ExportAllByTag(string templateName, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public async Task<List<IStatusHistory>> GetStatusHistoryWithUser(bool isThumbnailInclude, string applicationNumber, string productId = null)
        {
            var request = new RestRequest("getstatushistory/{applicationNumber}/{productId?}", Method.GET);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            request.AddUrlSegment(nameof(isThumbnailInclude), Convert.ToString(isThumbnailInclude));
            if (!string.IsNullOrEmpty(productId))
                request.AddUrlSegment(nameof(productId), Convert.ToString(productId));
            return await Client.ExecuteAsync<List<IStatusHistory>>(request);
        }

        public async Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus()
        {
            var request = new RestRequest("groupapplicationstatus/all", Method.GET);
            return await Client.ExecuteAsync<List<ApplicationStatusGroupView>>(request);
        }

        public async Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(int timeSpan)
        {
            var request = new RestRequest("groupapplicationstatus/{timeSpan}", Method.GET);
            request.AddUrlSegment("timeSpan", timeSpan.ToString());
            return await Client.ExecuteAsync<List<ApplicationStatusGroupView>>(request);
        }

        public async Task<IEnumerable<IApplicationStatusGroupView>> GetPartnerApplicationCountByStatus(string partnerId)
        {
            var request = new RestRequest("groupapplicationstatus/all/partner/{partnerId}", Method.GET);
            request.AddUrlSegment("partnerId", partnerId);
            return await Client.ExecuteAsync<List<ApplicationStatusGroupView>>(request);
        }

        public async Task<IEnumerable<IApplicationStatusGroupView>> GetPartnerApplicationCountByStatus(int timeSpan, string partnerId)
        {
            var request = new RestRequest("groupapplicationstatus/partner/{partnerId}/{timeSpan}", Method.GET);
            request.AddUrlSegment("timeSpan", timeSpan.ToString());
            request.AddUrlSegment("partnerId", partnerId);
            return await Client.ExecuteAsync<List<ApplicationStatusGroupView>>(request);
        }

        public Task<IAggregateFilterView> GetByApplicationNumberWithTags(string applicationNumber)
        {
            throw new NotImplementedException();
        }
    }
}