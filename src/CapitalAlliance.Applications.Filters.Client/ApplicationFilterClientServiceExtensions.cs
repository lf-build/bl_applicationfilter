﻿using LendFoundry.Security.Tokens;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else

using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace CapitalAlliance.Applications.Filters.Client
{
    public static class ApplicationFilterServiceExtension
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicationsFilterService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IApplicationFilterClientServiceFactory>(p => new ApplicationFilterClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationFilterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}