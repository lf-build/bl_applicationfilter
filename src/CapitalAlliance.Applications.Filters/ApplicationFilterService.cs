﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CapitalAlliance.Applications.Filters.Abstractions;
using CreditExchange.StatusManagement;
using System.Text;
using LendFoundry.DataAttributes;
using LendFoundry.VerificationEngine;
using LendFoundry.ProductRule;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Mvc;
using LendFoundry.Security.Identity.Client;
using Newtonsoft.Json;
using LendFoundry.EventHub;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;

#else

using Microsoft.AspNetCore.Mvc;
#endif

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationFilterService : IApplicationFilterServiceExtended
    {
        public ApplicationFilterService
        (
            IFilterViewRepository repository,
            IFilterViewTagsRepository tagsRepository,
            IEventHubClient eventHub,
            ILogger logger,
            ITenantTime tenantTime,
            Abstractions.Configurations.Configuration configuration,
            TaggingConfiguration taggingConfiguration,
            IProductRuleService productRuleService,
            IEntityStatusService statusManagementServiceClient,
            IDataAttributesEngine dataAttributesEngine,
            IVerificationEngineService verificationEngineService,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IIdentityService identityService,
             ICsvGenerator csvGenerator
        )
        {
            Repository = repository;
            TagsRepository = tagsRepository;
            EventHub = eventHub;
            Logger = logger;
            TenantTime = tenantTime;
            Configuration = configuration;
            TaggingConfiguration = taggingConfiguration;
            ProductRuleService = productRuleService;
            StatusManagementServiceClient = statusManagementServiceClient;
            DataAttributesEngine = dataAttributesEngine;
            VerificationEngineService = verificationEngineService;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            IdentityService = identityService;
            CsvGenerator = csvGenerator;
        }

        private Abstractions.Configurations.Configuration Configuration { get; }

        private TaggingConfiguration TaggingConfiguration { get; }

        private IFilterViewRepository Repository { get; }
        private IFilterViewTagsRepository TagsRepository { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private IProductRuleService ProductRuleService { get; }

        private IEntityStatusService StatusManagementServiceClient { get; }

        private IDataAttributesEngine DataAttributesEngine { get; }

        private IVerificationEngineService VerificationEngineService { get; }
        private IIdentityService IdentityService { get; }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }
        private ICsvGenerator CsvGenerator { get; set; }

        public IEnumerable<IFilterView> GetAll()
        {
            return Repository.GetAll();
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validate(sourceType, sourceId);

            return Repository.GetAllBySource(sourceType, sourceId);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetBySourceAndStatus(sourceType, sourceId, statuses);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfMonth = new TimeBucket(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, firstDayOfMonth);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfYear = new TimeBucket(new DateTime(TenantTime.Now.Year, 1, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentYear(sourceType, sourceId, firstDayOfYear);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByYear({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        private void Validate(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            try
            {
                Validate(sourceType, sourceId);

                if (string.IsNullOrWhiteSpace(name))
                    throw new InvalidArgumentException($"{nameof(name)} is mandatory");

                return Repository.GetByName(sourceType, sourceId, name);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByName({sourceType}, {sourceId}, {name}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetByStatus(sourceType, sourceId, statuses);
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetAllByStatus(statuses);
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetCountByStatus(sourceType, sourceId, statuses);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new ArgumentException($"{nameof(sourceType)} is mandatory");
            if (string.IsNullOrWhiteSpace(sourceId))
                throw new ArgumentException($"{nameof(sourceId)} is mandatory");
            if (Configuration?.ApprovedStatuses == null || Configuration.ApprovedStatuses.Any() == false)
                throw new ArgumentException($"{nameof(Configuration.ApprovedStatuses)} is mandatory in configuration");
            return 0.0; /*Repository.GetTotalAmountApprovedBySourceAndStatus(sourceType, sourceId, Configuration.ApprovedStatuses);*/
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validate(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var result = Repository.GetByApplicationNumber(sourceType, sourceId, applicationNumber);
            if (result.Result == null)
                throw new NotFoundException($"The snapshot could not be found with SourceType:{sourceType}, SourceId:{sourceId}, ApplicationNumber:{applicationNumber}");

            return result;
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber, string productId)
        {
            var filterDataView = await GetByApplicationNumber(sourceType, sourceId, applicationNumber);
            var statusHistory = await StatusManagementServiceClient.GetStatusTransitionHistory("application", applicationNumber, productId);
            filterDataView.StatusHistory = TransformHistoryToDictionary(statusHistory.ToList());
            return filterDataView;
        }

        //public async Task<List<IStatusHistory>> GetStatusHistoryWithUser(bool isThumbnailInclude, string applicationNumber, string productId)
        //{
        //    var statusHistory = await StatusManagementServiceClient.GetStatusTransitionHistory("application", applicationNumber,productId);
        //   // List<IStatusHistory> satusResponseList = MapActivityLog(statusHistory, isThumbnailInclude);
        //   // return satusResponseList;
        //}
        private List<IStatusHistory> MapActivityLog(IEnumerable<IEntityStatus> result)
        {
            List<IStatusHistory> statusResponseDetails = new List<IStatusHistory>();
            foreach (var item in result)
            {
                StatusHistory statusDetail = new StatusHistory();
                if (item != null)
                {
                    statusDetail.ActivatedBy = item.ActivatedBy;
                    statusDetail.Status = item.Status;
                    statusDetail.ActiveOn = item.ActivedOn;
                    statusDetail.Reason = item.Reason;
                }
                statusResponseDetails.Add(statusDetail);
            }

            return statusResponseDetails;
        }

        public async Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterName, string parameterValue)
        {
            if (string.IsNullOrEmpty(parameterValue))
                throw new ArgumentNullException($"required parameter {nameof(parameterValue)} missing");

            var result = false;
            var application = await Repository.GetUsingUniqueAttributes(parameterName, parameterValue);
            if (application != null &&
               Configuration.ReApplyStatuseCodes.Contains(application.StatusCode) &&
               DateTimeOffset.Now.Subtract(application.Submitted.Time).Days >= Configuration.ReApplyTimeFrameDays)
            {
                result = false;
            }
            else if (application != null)
            {
                result = true;
            }
            return result;
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var todayDate = new TimeBucket(TenantTime.Today);
            return await Repository.GetAllExpiredApplications(todayDate);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(List<string> excludedStatuses)
        {
            var todayDate = new TimeBucket(TenantTime.Today);
            return await Repository.GetAllExpiredApplications(todayDate, excludedStatuses);
        }

        public async Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityIds)
        {
            if (entityIds.ApplicationNumber.Count <= 0)
                throw new ArgumentNullException($"required parameter {nameof(entityIds)} null");
            return await Repository.GetApplicationsByEntityIds(entityIds);
        }

        #region "Tagging related methods"

        public async Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            IEnumerable<IFilterView> result = null;
            var taggedApplications = await TagsRepository.GetByTags(tags);

            if (taggedApplications != null)
            {
                var applicationNumbers = taggedApplications.Select(x => x.ApplicationNumber).ToList();
                result = await Repository.All(x => applicationNumbers.Contains(x.ApplicationNumber));
            }

            return result;
        }

        public Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            return TagsRepository.GetCountByTag(tags);
        }

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            return await TagsRepository.GetTagInfoForApplicationsByTag(tag);
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentNullException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            ApplyTags(applicationNumber, tags);
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentNullException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            RemoveTags(applicationNumber, tags);
        }

        public void ProcessTaggingEvent(EventInfo @event)
        {
            try
            {
                if (@event == null)
                    throw new InvalidArgumentException($"#{nameof(@event)} cannot be null", nameof(@event));

                var taggingEventConfiguration = TaggingConfiguration.Events.FirstOrDefault(x => x.Name == @event.Name);

                if (taggingEventConfiguration == null)
                    throw new ArgumentException($"Tagging event configuration for {@event.Name} not found");

                var applicationNumber = taggingEventConfiguration.ApplicationNumber.FormatWith(@event);
                Logger.Info($"Processing tagging event {@event.Name} for application {applicationNumber}");

                ApplyTags(applicationNumber, taggingEventConfiguration.ApplyTags);
                RemoveTags(applicationNumber, taggingEventConfiguration.RemoveTags);

                taggingEventConfiguration.Rules?.ToList().ForEach(async x =>
                {
                    var dataAttributes = await DataAttributesEngine.GetAllAttributes("application", applicationNumber);
                    var verificationDetails = await VerificationEngineService.GetFactVerification("application", applicationNumber);
                    var objActiveWorkFlow = await StatusManagementServiceClient.GetActiveStatusWorkFlow("application", applicationNumber);
                    var applicationStatusDetails = await StatusManagementServiceClient.GetStatusByEntity("application", applicationNumber, objActiveWorkFlow.StatusWorkFlowId);
                    var input = new { Data = @event.Data, DataAttributes = dataAttributes, VerificationDetails = verificationDetails, ApplicationStatus = applicationStatusDetails };
                    var ruleName = x.Name;
                    var version = x.Version;

                    try
                    {
                        ProductRuleResult ruleExecutionResult = null;
                        var result = await ProductRuleService.RunRule("application", applicationNumber, objActiveWorkFlow.ProductId, x.Name, input);

                        if (result != null)
                        {
                            ruleExecutionResult = result;
                            if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
                            {
                                throw new Exception("Unable to Run Rule TaggingRuleVerification");
                            }
                            if (ruleExecutionResult.IntermediateData != null)
                            {
                                var objTagResult = JsonConvert.DeserializeObject<TagResult>(ruleExecutionResult.IntermediateData.ToString());
                                if (objTagResult != null)
                                {
                                    Logger.Info($"{objTagResult.ApplyTags} to be applied to application {applicationNumber}");
                                    Logger.Info($"{objTagResult.RemoveTags} to be removed from application {applicationNumber}");

                                    ApplyTags(applicationNumber, objTagResult.ApplyTags);
                                    RemoveTags(applicationNumber, objTagResult.RemoveTags);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"Unhandled exception while processing application #{applicationNumber} for rule #{ruleName}.", ex);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method ProcessTaggingEvent({@event}) raised an error:{ex.Message}, Full Error:", ex);
            }
        }

        public IFilterViewTags GetApplicationTagInformation(string applicationNumber)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentNullException("Application Number required to fetch tag information");
            return TagsRepository.GetTagsForApplication(applicationNumber);
        }

        private void ApplyTags(string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    TagsRepository.AddTag(applicationNumber, x);
                }
            });
        }

        private void RemoveTags(string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    if (x == "ALL")
                    {
                        RemoveAllTags(applicationNumber);
                        return;
                    }
                    TagsRepository.RemoveTag(applicationNumber, x);
                }
            });
        }

        private void RemoveAllTags(string applicationNumber)
        {
            TagsRepository.ClearAllTags(applicationNumber);
        }

        #endregion "Tagging related methods"

        private Dictionary<string, object> TransformHistoryToDictionary(List<IEntityStatus> statusHistory)
        {
            Dictionary<string, object> statusHistoryDictionary = null;
            if (statusHistory != null && statusHistory.Count > 0)
            {
                statusHistoryDictionary = new Dictionary<string, object>();
                foreach (var status in statusHistory)
                {
                    StringBuilder strReason = new StringBuilder();
                    if (status.Reason != null && status.Reason.Count > 0)
                    {
                        foreach (var reason in status.Reason)
                        {
                            strReason.Append(reason + ",");
                        }
                    }

                    statusHistoryDictionary.Add(status.Status, new
                    {
                        ActivatedBy = status.ActivatedBy,
                        ActiveOn = status.ActivedOn,
                        Reasons = strReason.Length > 0 ? strReason.ToString().Substring(0, strReason.Length - 1) : string.Empty
                    });
                }
            }
            return statusHistoryDictionary;
        }

       

        #region Search Application

        public IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null)
        {
            IEnumerable<IFilterView> applicationSearchResult = Repository.SearchApplications(searchParams, condition);

            return applicationSearchResult;
        }

        #endregion Search Application

        #region assignment filters

        public async Task<IEnumerable<IFilterView>> GetAllUnassignedByUser()
        {
            var roles = await GetUserRoles();

            return await Repository.GetAllUnassignedByUser(roles);
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassigned()
        {
            return await Repository.GetAllUnassigned();
        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            return await Repository.GetAllAssignedApplications(userName);
        }

        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }

        private async Task<IEnumerable<string>> GetUserRoles()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            var roles = await IdentityService.GetUserRoles(userName);
            if (roles == null || roles.Any() == false)
                throw new NotFoundException("Could not be found the roles from current user");

            return roles;
        }

        #endregion assignment filters

        #region Export Application

        public FileContentResult ExportAll(string templateName)
        {
            if (string.IsNullOrEmpty(templateName))
                throw new ArgumentException($"{nameof(templateName)} is mandatory");

            var result = GetAll();
            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault(x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient(CsvGenerator.WriteToCsv(result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString("dd-MM-yyyy") + ".csv");
        }

        public async Task<FileContentResult> ExportAllByStatus(string templateName, IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            if (string.IsNullOrEmpty(templateName))
                throw new ArgumentException($"{nameof(templateName)} is mandatory");

            var result = await Repository.GetAllByStatus(statuses);
            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault(x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient(CsvGenerator.WriteToCsv(result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString("dd-MM-yyyy") + ".csv");
        }

        public async Task<FileContentResult> ExportAllByTag(string templateName, IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            if (string.IsNullOrEmpty(templateName))
                throw new ArgumentException($"{nameof(templateName)} is mandatory");

            IEnumerable<IFilterView> result = null;
            var taggedApplications = await TagsRepository.GetByTags(tags);

            if (taggedApplications != null)
            {
                var applicationNumbers = taggedApplications.Select(x => x.ApplicationNumber).ToList();
                result = await Repository.All(x => applicationNumbers.Contains(x.ApplicationNumber));
            }

            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault(x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient(CsvGenerator.WriteToCsv(result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString("dd-MM-yyyy") + ".csv");
        }

        private FileContentResult SendFileToClient(byte[] exportStream, string fileName)
        {
            FileContentResult result = new FileContentResult(exportStream, "application/octet-stream")
            {
                FileDownloadName = fileName
            };
            return result;
        }

        #endregion Export Application

        #region Metric Collector Agrregate Quries

        public Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(int timeSpan, string partnerId = null)
        {
            if (timeSpan <= 0)
            {
                throw new ArgumentException("Time period must be greater than zero");
            }
            DateTimeOffset fromDate = TenantTime.Now.UtcDateTime.AddDays(-timeSpan);
            DateTimeOffset toDate = TenantTime.Now.UtcDateTime;
            return Repository.GetApplicationCountByStatus(fromDate, toDate, partnerId);
        }

        public Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus()
        {
            return Repository.GetApplicationCountByStatus();
        }

        public Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(int timeSpan)
        {
            if (timeSpan <= 0)
            {
                throw new ArgumentException("Time period must be greater than zero");
            }
            DateTimeOffset fromDate = TenantTime.Now.UtcDateTime.AddDays(-timeSpan);
            DateTimeOffset toDate = TenantTime.Now.UtcDateTime;
            return Repository.GetApplicationCountByStatus(fromDate, toDate);
        }

        public Task<IEnumerable<IApplicationStatusGroupView>> GetPartnerApplicationCountByStatus(string partnerId)
        {
            return Repository.GetApplicationCountByStatus(partnerId);
        }

        Task<IEnumerable<IApplicationStatusGroupView>> IApplicationFilterService.GetPartnerApplicationCountByStatus(int timeSpan, string partnerId)
        {
            if (timeSpan <= 0)
            {
                throw new ArgumentException("Time period must be greater than zero");
            }
            DateTimeOffset fromDate = TenantTime.Now.UtcDateTime.AddDays(-timeSpan);
            DateTimeOffset toDate = TenantTime.Now.UtcDateTime;
            return Repository.GetApplicationCountByStatus(fromDate, toDate, partnerId);
        }

        #endregion Metric Collector Agrregate Quries

        public async Task<IAggregateFilterView> GetByApplicationNumberWithTags(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));
            var filterView = await Repository.GetByApplicationNumber(applicationNumber);
            if (filterView == null)
                throw new NotFoundException($"The snapshot could not be found for ApplicationNumber:{applicationNumber}");
            var filterViewTags = TagsRepository.GetTagsForApplication(applicationNumber);
            return new AggregateFilterView(filterView, filterViewTags);
        }

        public Task<List<IStatusHistory>> GetStatusHistoryWithUser(bool isThumbnailInclude, string applicationNumber, string productId)
        {
            throw new NotImplementedException();
        }
    }
}