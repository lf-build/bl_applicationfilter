﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
//using Microsoft.AspNet.Builder;
using Microsoft.Extensions.DependencyInjection;

#if DOTNET2
using Microsoft.AspNetCore.Builder;

#else

using Microsoft.AspNet.Builder;
#endif

namespace CapitalAlliance.Applications.Filters
{
    public static class ApplicationFilterListenerExtensions
    {
        public static void UseApplicationFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationFilterListener>().Start();
        }
    }
}