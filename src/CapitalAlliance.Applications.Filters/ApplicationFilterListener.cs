﻿using System;
using System.Collections.Generic;
using System.Linq;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement;
using CreditExchange.StatusManagement.Client;
using LendFoundry.AssignmentEngine;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Funding;
using LendFoundry.Business.Funding.Client;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Partner;
using LendFoundry.Partner.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Configuration = CapitalAlliance.Applications.Filters.Abstractions.Configurations.Configuration;
using LendFoundry.EventHub;

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationFilterListener : IApplicationFilterListener
    {
        public ApplicationFilterListener
            (
                IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                IFilterViewRepositoryFactory repositoryFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                IApplicationServiceClientFactory applicationServiceFactory,
                IApplicantServiceClientFactory applicantServiceFactory,
                IStatusManagementServiceFactory statusManagementFactory,
                IConfigurationServiceFactory configurationFactory,
                ITenantTimeFactory tenantTimeFactory,
                IConfigurationServiceFactory<TaggingConfiguration> apiTaggingConfigurationFactory,
                IApplicationFilterServiceFactory applicationFilterServiceFactory,
                IAssignmentServiceClientFactory assignmentFactory,
                IIdentityServiceFactory identityServiceFactory,
                IOfferEngineServiceClientFactory offerEngineServiceFactory,
                IPartnerServiceFactory partnerServiceFactory,
                IFundingServiceClientFactory fundingServiceClientFactory
            )
        {
            EventHubFactory = eventHubFactory;
            ApiConfigurationFactory = apiConfigurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationServiceFactory = applicationServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            StatusManagementFactory = statusManagementFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            ApiTaggingConfigurationFactory = apiTaggingConfigurationFactory;
            ApplicationFilterServiceFactory = applicationFilterServiceFactory;
            AssignmentFactory = assignmentFactory;
            IdentityServiceFactory = identityServiceFactory;
            OfferEngineServiceFactory = offerEngineServiceFactory;
            PartnerServiceFactory = partnerServiceFactory;
            FundingServiceClientFactory = fundingServiceClientFactory;
        }
        private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }
        private IApplicantServiceClientFactory ApplicantServiceFactory { get; }
        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }
        private IConfigurationServiceFactory<TaggingConfiguration> ApiTaggingConfigurationFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IFilterViewRepositoryFactory RepositoryFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IApplicationFilterServiceFactory ApplicationFilterServiceFactory { get; }
        private static Object Key = new object();
        private IAssignmentServiceClientFactory AssignmentFactory { get; }
        private IIdentityServiceFactory IdentityServiceFactory { get; }
        private IOfferEngineServiceClientFactory OfferEngineServiceFactory { get; }
        private IPartnerServiceFactory PartnerServiceFactory { get; }
        private IFundingServiceClientFactory FundingServiceClientFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);

                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    // Tenant token creation
                    var token = TokenHandler.Issue(tenant.Id, "application-filters");
                    var reader = new StaticTokenReader(token.Value);
                    var hub = EventHubFactory.Create(reader);
                    // Needed resources for this operation
                    var statusManagementService = StatusManagementFactory.Create(reader);
                    var repository = RepositoryFactory.Create(reader);
                    var applicationService = ApplicationServiceFactory.Create(reader);
                    var applicantService = ApplicantServiceFactory.Create(reader);
                    var configuration = ApiConfigurationFactory.Create(reader).Get();
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var taggingConfiguration = ApiTaggingConfigurationFactory.Create(reader).Get();
                    var assignmentService = AssignmentFactory.Create(reader);
                    var offerEngineService = OfferEngineServiceFactory.Create(reader);
                    var partnerService = PartnerServiceFactory.Create(reader);
                    var fundingService = FundingServiceClientFactory.Create(reader);

                    if (configuration == null)
                        throw new ArgumentException("Api configuration cannot be found, please check");

                    var applicationFilterService = ApplicationFilterServiceFactory.Create(reader, logger, TokenHandler);

                    var identityService = IdentityServiceFactory.Create(reader);
                    // Attach all configured events to be listen
                    configuration
                        .Events
                        .ToList()
                        .ForEach(eventConfig =>
                        {
                            hub.On(eventConfig.Name, AddView(eventConfig, repository,
                                logger, applicationService, applicantService,
                                statusManagementService, tenantTime, taggingConfiguration, applicationFilterService, assignmentService, identityService, offerEngineService, partnerService, hub, fundingService, configuration));
                            logger.Info($"Event #{eventConfig.Name} attached to listener");
                        });
                    hub.StartAsync();
                });
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }

        private static Action<EventInfo> AddView(
            Abstractions.Configurations.EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            LendFoundry.Business.Application.IApplicationService applicationService,
            IApplicantService applicantService,
            IEntityStatusService statusManagementService,
            ITenantTime tenantTime,
            TaggingConfiguration taggingConfiguration,
            IApplicationFilterServiceExtended applicationFilterService,
            IAssignmentService assignmentService,
            IIdentityService identityService,
            IOfferEngineService offerEngineService,
            IPartnerService partnerServiceService,
            IEventHubClient eventHubService,
            IFundingService FundingServiceService,
            Configuration configuration

        )
        {
            return async @event =>
            {
                try
                {
                    logger.Info($"       Now processing {           @event.Name} with payload {@event.Data}");

                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);

                    logger.Info($"     Application Number: #{ applicationNumber }        ");

                    var application = await applicationService.GetByApplicationNumber(applicationNumber);

                    if (application == null)
                    {
                        logger.Warn($"                No application found                for application# { applicationNumber }                ");
                        return;
                    }

                    var applicant = await applicantService.Get(application.ApplicantId);

                    if (applicant == null)
                    {
                        logger.Warn($"                No applicants found                for application# { applicationNumber }                ");
                        return;
                    }

                    // snapshot creation
                    var data = new FilterView();
                    data.ContactFirstName = application.ContactFirstName;
                    data.ContactLastName = application.ContactLastName;
                    data.BusinessApplicantName = $"{ application.ContactFirstName } { application.ContactLastName }";
                    data.DBA = applicant.DBA;
                    data.EIN = applicant.EIN;
                    data.SICCode = applicant.SICCode;
                    data.DUNSNumber = applicant.DUNSNumber;
                    data.NAICCode = applicant.NAICCode;
                    data.ProductId = application.ProductId;
                    data.PortfolioType = application.PortfolioType;
                    data.ProductCategory = application.ProductCategory;
                    data.PropertyType = applicant.PropertyType;
                    data.Industry = applicant.Industry;
                    data.ApplicantId = application.ApplicantId;
                    data.ApplicationNumber = application.ApplicationNumber;
                    data.OldProductId = application.OldProductId;

                    if (application.OldProductId != null)
                    {
                        if (Convert.ToString(application.OldProductId).ToLower().Contains("loc"))
                            data.OldProductDisplayName = "StreamLine";
                        else
                            data.OldProductDisplayName = "Traditional";
                    }
                    else
                    {
                        data.OldProductDisplayName = "Traditional";
                    }
                    if (application.ProductId != null)
                    {
                        if (application.ProductId.ToLower().Contains("loc"))
                            data.ProductDisplayName = "StreamLine";
                        else
                            data.ProductDisplayName = "Traditional";
                    }
                    else
                    {
                        data.ProductDisplayName = "Traditional";

                    }


                    data.OldPortfolioType = application.OldPortfolioType;
                    data.OldProductCategory = application.OldProductCategory;
                    data.LoanTimeFrame = application.LoanTimeFrame;
                    data.DateNeeded = application.DateNeeded;
                    data.Submitted = application.ApplicationDate;
                    data.ApplicationDate = application.ApplicationDate;
                    data.IPAddress = application?.ClientIpAddress;
                    data.LoanPriority = applicant.LoanPriority;
                    data.BusinessLocation = applicant.BusinessLocation;

                    if (application.SelfDeclareInformation != null)
                    {
                        data.AnnualRevenue = application.SelfDeclareInformation.AnnualRevenue;
                        data.AverageBankBalance = application.SelfDeclareInformation.AverageBankBalance;
                        data.IsExistingBusinessLoan = application.SelfDeclareInformation.IsExistingBusinessLoan;
                    }
                    if (eventConfiguration.UpdateLastProgressDate)
                    {
                        data.LastProgressDate = new TimeBucket(tenantTime.Now);
                    }
                    data.RequestedAmount = application.RequestedAmount;
                    data.RequestedTermType = application.RequestedTermType;
                    data.RequestedTermValue = application.RequestedTermValue;
                    data.PurposeOfLoan = application.PurposeOfLoan;
                    data.DecisionDate = application.DecisionDate;
                    data.ExpiryDate = application.ExpiryDate;
                    data.ExternalReferences = application.ExternalReferences;
                    data.TotalDrawDownAmount = application.TotalDrawDownAmount;

                    //if (applicant.Addresses != null && applicant.Addresses.Count > 0)
                    // {
                    var businessAddress = applicant.PrimaryAddress;//                            applicant.PrimaryAddress.FirstOrDefault(x => x.AddressType == LendFoundry.Business.Applicant.AddressType.Business);

                    if (businessAddress != null)
                    {
                        data.BusinessAddressLine1 = businessAddress.AddressLine1;
                        data.BusinessAddressLine2 = businessAddress.AddressLine2;
                        data.BusinessAddressLine3 = businessAddress.AddressLine3;
                        data.BusinessAddressLine4 = businessAddress.AddressLine4;
                        data.BusinessCity = businessAddress.City;
                        data.BusinessState = businessAddress.State;
                        data.BusinessZipCode = businessAddress.ZipCode;
                        data.BusinessCountry = businessAddress.Country;
                    }
                    //}
                    if (data.SocialLinks != null)
                    {
                        data.SocialLinks = new List<string>();
                        foreach (var sociallink in data.SocialLinks)
                        {
                            data.SocialLinks.Add(sociallink);
                        }
                    }
                    data.BusinessFax = applicant.PrimaryFax;
                    data.BusinessWebsite = applicant.BusinessWebsite;
                    data.BusinessStartDate = applicant.BusinessStartDate;
                    data.BusinessTaxID = applicant.BusinessTaxID;
                    data.PhoneNumber = application.PrimaryPhone.Phone;
                    data.BusinessPhone = application.BusinessPhone?.Phone != null ? application.BusinessPhone?.Phone : application.PrimaryPhone.Phone;
                    data.BusinessEmail = application.PrimaryEmail.Email;
                    data.LegalBusinessName = applicant.LegalBusinessName;
                    data.LoanAmount = application.LoanAmount;
                    data.BusinessType = applicant.BusinessType;

                    if (application.Source != null)
                    {
                        data.SourceReferenceId = application.Source.SourceReferenceId;
                        data.SourceType = application.Source.SourceType?.ToString();
                    }
                    if (applicant.Owners != null && applicant.Owners.Count > 0)
                    {
                        data.Owners = new List<IOwner>();
                        for (int i = 0; i < applicant.Owners.Count; i++)
                        {
                            IOwner objOwner = new Owner();

                            if (applicant.Owners[i].Addresses != null && applicant.Owners[i].Addresses.Count > 0)
                            {
                                objOwner.AddressLine1 = applicant.Owners[i].Addresses[0].AddressLine1;
                                objOwner.AddressLine2 = applicant.Owners[i].Addresses[0].AddressLine2;
                                objOwner.AddressLine3 = applicant.Owners[i].Addresses[0].AddressLine3;
                                objOwner.AddressLine4 = applicant.Owners[i].Addresses[0].AddressLine4;
                                objOwner.City = applicant.Owners[i].Addresses[0].City;
                                objOwner.State = applicant.Owners[i].Addresses[0].State;
                                objOwner.ZipCode = applicant.Owners[i].Addresses[0].ZipCode;
                                objOwner.Country = applicant.Owners[i].Addresses[0].Country;
                            }
                            objOwner.Designation = applicant.Owners[i].Designation;
                            objOwner.DOB = applicant.Owners[i].DOB;
                            objOwner.OwnerId = applicant.Owners[i].OwnerId;
                            if (applicant.Owners[i].EmailAddresses != null && applicant.Owners[i].EmailAddresses.Count > 0)
                            {
                                objOwner.EmailAddress = applicant.Owners[i].EmailAddresses[0].Email;
                            }
                            objOwner.FirstName = applicant.Owners[i].FirstName;
                            objOwner.LastName = applicant.Owners[i].LastName;
                            objOwner.OwnershipPercentage = applicant.Owners[i].OwnershipPercentage;
                            objOwner.OwnershipType = applicant.Owners[i].OwnershipType + "";

                            /*if (applicant.Owners[i].PhoneNumbers != null && applicant.Owners[i].PhoneNumbers.Count > 0)
                            {
                                objOwner.PhoneNumber = applicant.Owners[i].PhoneNumbers[0].Phone;
                            }*/

                            if (applicant.Owners[i].PhoneNumbers != null && applicant.Owners[i].PhoneNumbers.Count > 0)
                            {
                                var phoneNumbers = applicant.Owners[i].PhoneNumbers.FirstOrDefault(x => x.PhoneType == LendFoundry.Business.Applicant.PhoneType.Residence);

                                objOwner.PhoneNumber = phoneNumbers?.Phone != null ? phoneNumbers?.Phone : applicant.Owners[i].PhoneNumbers[0].Phone;

                                var mobileNumbers = applicant.Owners[i].PhoneNumbers.FirstOrDefault(x => x.PhoneType == LendFoundry.Business.Applicant.PhoneType.Mobile);

                                objOwner.MobileNumber = mobileNumbers?.Phone;
                            }
                            objOwner.SSN = applicant.Owners[i].SSN;
                            objOwner.IsPrimary = applicant.Owners[i].IsPrimary;
                            data.Owners.Add(objOwner);
                        }
                    }

                    var Offers = await offerEngineService.GetApplicationOffers("application", applicationNumber);

                    if (Offers != null && Offers.DealOffer != null)
                    {
                        data.AccountNumber = Offers.DealOffer.AccountNumber;
                        data.AccountType = Offers.DealOffer.AccountType;
                        data.ACHFee = Offers.DealOffer.ACHFee;
                        data.AfterDefaultAssumption = Offers.DealOffer.AfterDefaultAssumption;
                        data.AmountFunded = Offers.DealOffer.AmountFunded;
                        data.ApprovedAmount = Offers.DealOffer.ApprovedAmount;
                        data.AverageLife = Offers.DealOffer.AverageLife;
                        data.BankName = Offers.DealOffer.BankName;
                        data.BuyRate = Offers.DealOffer.BuyRate;
                        data.CommissionAmount = Offers.DealOffer.CommissionAmount;
                        data.CommissionRate = Offers.DealOffer.CommissionRate;
                        data.DurationType = Offers.DealOffer.DurationType;
                        data.Grade = Offers.DealOffer.Grade;
                        data.IRR = Offers.DealOffer.IRR;
                        data.LenderReturn = Offers.DealOffer.LenderReturn;

                        data.NetFundingRate = Offers.DealOffer.NetFundingRate;
                        data.NumberOfPayment = Offers.DealOffer.NumberOfPayment;
                        data.OriginatingFeeAmount = Offers.DealOffer.OriginatingFeeAmount;
                        data.PaymentAmount = Offers.DealOffer.PaymentAmount;
                        data.RepaymentAmount = Offers.DealOffer.RepaymentAmount;
                        data.RoutingNumber = Offers.DealOffer.RoutingNumber;
                        data.SellRate = Offers.DealOffer.SellRate;
                        data.Term = Offers.DealOffer.Term;
                        data.TermPayment = Offers.DealOffer.TermPayment;
                        data.TypeOfPayment = Offers.DealOffer.TypeOfPayment;
                    }

                    //Calculate ClosureDate

                    var statusInfo = statusManagementService.GetStatusTransitionHistory("application", application.ApplicationNumber, application.ProductId).Result;

                    if (statusInfo.ToList().Any(x => x.Status == configuration.OpenStatusCode))
                    {
                        var openStatusInfo = statusInfo.FirstOrDefault(x => x.Status == configuration.OpenStatusCode);
                        DateTimeOffset? termClouserDate = CalculateClosureDate(configuration, application, openStatusInfo);
                        data.TermClosureDate = termClouserDate != null ? new TimeBucket(termClouserDate.Value) : null;
                        data.OpenDate = openStatusInfo.ActivedOn;
                    }

                    // gets all application assignees
                    var assignees = new Dictionary<string, string>();
                    IEnumerable<IAssignment> assignments = null;

                    try
                    {
                        assignments = await assignmentService.Get("Application", application.ApplicationNumber);
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"            assignmentService error: { ex.Message }                        ");
                    }

                    if (assignments == null || assignments.Any() == false)
                        logger.Warn($"                No assignments found                for this snapshot using EntityId# { application.ApplicationNumber }                ");

                    if (assignments != null && assignments.Any())
                        assignees = assignments.ToDictionary(x => x.Role, x => x.Assignee);

                    data.Assignees = assignees;

                    var objActiveWorkFlow = await statusManagementService.GetActiveStatusWorkFlow("application", applicationNumber);
                    var applicationStatus = await statusManagementService.GetStatusByEntity("application", applicationNumber, objActiveWorkFlow.StatusWorkFlowId);

                    if (applicationStatus != null)
                    {
                        data.StatusCode = applicationStatus.Code;
                        if (applicationStatus.ActiveOn != null)
                            data.StatusDate = new TimeBucket(applicationStatus.ActiveOn.Time);
                        data.StatusName = applicationStatus.Name;
                        data.DeclineReason = applicationStatus.ReasonsSelected;
                        data.SubStatusDetail = applicationStatus.SubStatusDetail;
                        data.StatusWorkFlowId = applicationStatus.StatusWorkFlowId;
                        data.WorkFlowStatus = applicationStatus.WorkFlowStatus;
                        logger.Info($"    Status found              for this snapshot# { data.StatusCode }# { data.StatusName }               ");
                    }
                    else
                        logger.Info($"                No status found                for this snapshot ");

                    try
                    {
                        if (!string.IsNullOrWhiteSpace(application?.LeadOwnerId))
                        {
                            var leadOwner = await identityService.GetUserById(application.LeadOwnerId);
                            data.LeadOwnerUsername = leadOwner?.Username;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"                identityService error: { ex.Message }                ");
                    }
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(application?.PartnerId))
                        {
                            data.PartnerUserId = application.PartnerUserId; //GUID
                            data.PartnerId = application.PartnerId;   //PartnerID
                            var Partner = await partnerServiceService.Get(application.PartnerId);
                            data.PartnerName = Partner.Name;
                            data.PartnerEmail = Partner.Contacts[0].Email;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"                identityService error: { ex.Message }                ");
                    }

                    IApplicationFunding fundingData = null;

                    try
                    {
                        fundingData = await FundingServiceService.GetFundingData("application", applicationNumber);
                        if (fundingData != null && fundingData.ReferenceId != null)
                        {
                            data.FundingReferenceId = fundingData.ReferenceId;
                            data.FundingRequestStatus = fundingData.RequestStatus;
                            data.FundingRequestedDate = fundingData.RequestedDate;

                            var FundingAttemptData = await FundingServiceService.GetAttemptData(fundingData.ReferenceId);

                            if (FundingAttemptData != null && FundingAttemptData.Any())
                            {
                                var FundingAttemptDataLatest = FundingAttemptData.OrderByDescending(x => x.AttemptDate.Time).FirstOrDefault();

                                if (FundingAttemptDataLatest != null)
                                {
                                    data.FundingReturnCode = FundingAttemptDataLatest.ReturnCode;
                                    data.FundingReturnDate = FundingAttemptDataLatest.ReturnDate;
                                    data.FundingReturnReason = FundingAttemptDataLatest.ReturnReason;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"             fundingService error: { ex.Message }                ");
                    }

                    if (taggingConfiguration.Events.Exists(x => x.Name.ToLower() == eventConfiguration.Name.ToLower()))
                    {
                        applicationFilterService.ProcessTaggingEvent(@event);
                    }

                    var tagDetails = applicationFilterService.GetApplicationTagInformation(applicationNumber);

                    if (tagDetails != null)
                    {
                        data.Tags = tagDetails.Tags;
                    }

                    lock (Key)
                    {
                        repository.AddOrUpdate(data);
                    }
                    await eventHubService.Publish(new FilterDataUpdated { EventName = @event.Name, Data = data });
                    logger.Info($"                New snapshot added to application { applicationNumber }             ");
                }
                catch (Exception ex)
                {
                    logger.Error($"                Unhadled exception                while listening event {                    @event.Name}", ex);
                }
            };
        }

        private static DateTimeOffset? CalculateClosureDate(Configuration configuration, LendFoundry.Business.Application.IApplication application, IEntityStatus statusInfo)
        {
            var openApplicationDate = statusInfo.ActivedOn;
            var requestedTermType = application.RequestedTermType;
            var termValue = application.RequestedTermValue;
            DateTimeOffset? termClouserDate = null;

            if (requestedTermType != null)
            {
                switch (requestedTermType.ToLower())
                {
                    case "daily":
                        termClouserDate = openApplicationDate.Time.AddDays(termValue);
                        break;

                    case "weekly":
                        termClouserDate = openApplicationDate.Time.AddDays(termValue * 7);
                        break;

                    case "biweekly":
                        termClouserDate = openApplicationDate.Time.AddDays(termValue * 14);
                        break;

                    case "monthly":
                        termClouserDate = openApplicationDate.Time.AddMonths(Convert.ToInt16(termValue));
                        break;

                    case "yearly":
                        termClouserDate = openApplicationDate.Time.AddYears(Convert.ToInt16(termValue));
                        break;
                }
            }
            return termClouserDate;
        }

        private static string GetOwnerProperty(int index, string name)
        {
            if (name != String.Empty)
            {
                return "Owner" + index + name;
            }
            return name;
        }

        private static void SetOwnerProperty(FilterView data, string owenerpropertyName, string OwnerListPropValue)
        {
            if (OwnerListPropValue != null)
            {
                System.Reflection.PropertyInfo prop = typeof(IFilterView).GetProperty(owenerpropertyName);
                prop.SetValue(data, OwnerListPropValue, null);
            }
        }
    }
}