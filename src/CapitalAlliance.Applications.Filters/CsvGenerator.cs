﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CapitalAlliance.Applications.Filters
{
    public class CsvGenerator : ICsvGenerator
    {
        public string Delimiter { get; set; } = "|";
        private Dictionary<string, string> CsvProjection { get; set; }

        public byte[] WriteToCsv<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            try
            {
                CsvProjection = csvProjection;
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues()))
                    {
                        csvWriter.WriteRecords(items.Cast<FilterView>());
                    }
                    return memoryStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8
            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            csvConfiguration.Delimiter = Delimiter;
            return csvConfiguration;
        }

        private CsvClassMap SetCsvClassMap()
        {
            var FileMap = new DefaultCsvClassMap<FilterView>();
            CsvClassMap csvFileMap;
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;

                if (!String.IsNullOrEmpty(csvColumnName))
                {
                    var propertyInfo = typeof(FilterView).GetProperty(schemaPropertyName);
                    var newMap = new CsvPropertyMap(propertyInfo);
                    newMap.Name(csvColumnName);
                    newMap.Index(columnIndex);
                    FileMap.PropertyMaps.Add(newMap);
                    columnIndex += 1;
                }
            }

            csvFileMap = FileMap;
            return csvFileMap;
        }
    }
}
