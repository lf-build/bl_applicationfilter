﻿using System;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.VerificationEngine.Client;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.Applications.Filters {
    public class ApplicationFilterServiceFactory : IApplicationFilterServiceFactory {
        public ApplicationFilterServiceFactory (IServiceProvider provider) {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IApplicationFilterServiceExtended Create (ITokenReader reader, ILogger logger, ITokenHandler handler) {
            var repositoryFactory = Provider.GetService<IFilterViewRepositoryFactory> ();
            var repository = repositoryFactory.Create (reader);
            var repositoryTagsFactory = Provider.GetService<IFilterViewTagsRepositoryFactory> ();
            var tagsRepository = repositoryTagsFactory.Create (reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory> ();
            var eventHub = eventHubFactory.Create (reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory> ();
            var taggingConfigurationService = configurationServiceFactory.Create<TaggingConfiguration> (CapitalAlliance.Applications.Filters.Abstractions.Settings.TaggingEvents, reader);
            var taggingConfiguration = taggingConfigurationService.Get ();

            var configurationService = configurationServiceFactory.Create<Configuration> (CapitalAlliance.Applications.Filters.Abstractions.Settings.ServiceName, reader);
            var configuration = configurationService.Get ();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory> ();
            var tenantTime = tenantTimeFactory.Create (configurationServiceFactory, reader);

            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory> ();
            var productRuleEngine = productRuleFactory.Create (reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory> ();
            var statusService = statusManagementServiceFactory.Create (reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory> ();
            var dataAttributesEngineService = dataAttributesClientFactory.Create (reader);

            var verficationEngineServiceFactory = Provider.GetService<IVerificationEngineServiceClientFactory> ();
            var verificationEngineService = verficationEngineServiceFactory.Create (reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory> ();
            var identityEngineService = identityServiceFactory.Create (reader);

            ICsvGenerator csvGenerator = new CsvGenerator ();
            return new ApplicationFilterService (repository, tagsRepository, eventHub, logger, tenantTime, configuration, taggingConfiguration, productRuleEngine, statusService, dataAttributesEngineService, verificationEngineService, reader, handler, identityEngineService, csvGenerator);
        }
    }
}