﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Business.Application;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class AggregateFilterView : IAggregateFilterView
    {
        public AggregateFilterView(IFilterView filterView, IFilterViewTags tags)
        {
            if (filterView != null)
            {
                IsExistingBusinessLoan = filterView.IsExistingBusinessLoan;
                ApplicationDate = filterView.ApplicationDate;
                BusinessStartDate = filterView.BusinessStartDate;
                DateNeeded = filterView.DateNeeded;
                DecisionDate = filterView.DecisionDate;
                ExpiryDate = filterView.ExpiryDate;
                LastProgressDate = filterView.LastProgressDate;
                StatusDate = filterView.StatusDate;
                Submitted = filterView.Submitted;
                StatusHistory = filterView.StatusHistory;
                AnnualRevenue = filterView.AnnualRevenue;
                AverageBankBalance = filterView.AverageBankBalance;
                RequestedAmount = filterView.RequestedAmount;
                RequestedTermValue = filterView.RequestedTermValue;
                SocialLinks = filterView.SocialLinks;
                ApplicantId = filterView.ApplicantId;
                ApplicationNumber = filterView.ApplicationNumber;
                BusinessAddressLine1 = filterView.BusinessAddressLine1;
                BusinessAddressLine2 = filterView.BusinessAddressLine2;
                BusinessAddressLine3 = filterView.BusinessAddressLine3;
                BusinessAddressLine4 = filterView.BusinessAddressLine4;
                BusinessApplicantName = filterView.BusinessApplicantName;
                BusinessCity = filterView.BusinessCity;
                BusinessCountry = filterView.BusinessCountry;
                BusinessCountryCode = filterView.BusinessCountryCode;
                BusinessEmail = filterView.BusinessEmail;
                BusinessFax = filterView.BusinessFax;
                BusinessLandMark = filterView.BusinessLandMark;
                BusinessLocation = filterView.BusinessLocation;
                BusinessPhone = filterView.BusinessPhone;
                BusinessState = filterView.BusinessState;
                BusinessTaxID = filterView.BusinessTaxID;
                BusinessType = filterView.BusinessType;
                BusinessWebsite = filterView.BusinessWebsite;
                BusinessZipCode = filterView.BusinessZipCode;
                ContactFirstName = filterView.ContactFirstName;
                ContactLastName = filterView.ContactLastName;
                DBA = filterView.DBA;
                DUNSNumber = filterView.DUNSNumber;
                LegalBusinessName = filterView.LegalBusinessName;
                LoanTimeFrame = filterView.LoanTimeFrame;
                NAICCode = filterView.NAICCode;
                OtherPurposeDescription = filterView.OtherPurposeDescription;
                ProductId = filterView.ProductId;
                PropertyType = filterView.PropertyType;
                PurposeOfLoan = filterView.PurposeOfLoan;
                RequestedTermType = filterView.RequestedTermType;
                SICCode = filterView.SICCode;
                SourceReferenceId = filterView.SourceReferenceId;
                SourceType = filterView.SourceType;
                StatusCode = filterView.StatusCode;
                StatusName = filterView.StatusName;
                TrackingCode = filterView.TrackingCode;
                Industry = filterView.Industry;
                EIN = filterView.EIN;
                Assignees = filterView.Assignees;
                LeadOwnerUsername = filterView.LeadOwnerUsername;
                DeclineReason = filterView.DeclineReason;
                Owners = filterView.Owners;
                LoanAmount = filterView.LoanAmount;
                RepaymentAmount = filterView.RepaymentAmount;
                SellRate = filterView.SellRate;
                CommissionRate = filterView.CommissionRate;
                CommissionAmount = filterView.CommissionAmount;
                DurationType = filterView.DurationType;
                TermPayment = filterView.TermPayment;
                AmountFunded = filterView.AmountFunded;
                BankName = filterView.BankName;
                AccountNumber = filterView.AccountNumber;
                AccountType = filterView.AccountType;
                RoutingNumber = filterView.RoutingNumber;
                Term = filterView.Term;
                ApprovedAmount = filterView.ApprovedAmount;
                TypeOfPayment = filterView.TypeOfPayment;
                PaymentAmount = filterView.PaymentAmount;
                NumberOfPayment = filterView.NumberOfPayment;
                OriginatingFeeAmount = filterView.OriginatingFeeAmount;
                ACHFee = filterView.ACHFee;
                BuyRate = filterView.BuyRate;
                LenderReturn = filterView.LenderReturn;
                NetFundingRate = filterView.NetFundingRate;
                AfterDefaultAssumption = filterView.AfterDefaultAssumption;
                AverageLife = filterView.AverageLife;
                IRR = filterView.IRR;
                Grade = filterView.Grade;
                PartnerUserId = filterView.PartnerUserId;
                PartnerId = filterView.PartnerId;
                PartnerName = filterView.PartnerName;
                PartnerEmail = filterView.PartnerEmail;
                FundingRequestedDate = filterView.FundingRequestedDate;
                FundingReferenceId = filterView.FundingReferenceId;
                FundingRequestStatus = filterView.FundingRequestStatus;
                FundingReturnDate = filterView.FundingReturnDate;
                FundingReturnCode = filterView.FundingReturnCode;
                FundingReturnReason = filterView.FundingReturnReason;
                ExternalReferences = filterView.ExternalReferences;
                IPAddress = filterView.IPAddress;
                ProductDisplayName = filterView.ProductDisplayName;
                OldProductDisplayName = filterView.OldProductDisplayName;
                LoanPriority = filterView.LoanPriority;
                PhoneNumber = filterView.PhoneNumber;
            }
            if (tags != null && tags.Tags != null)
            {
                Tags = tags.Tags.Select(tag => tag.TagName);
            }
        }
        public string LoanPriority { get; set; }
        public string PhoneNumber { get; set; }
        public string ProductDisplayName { get; set; }
        public string OldProductDisplayName { get; set; }
        public bool IsExistingBusinessLoan { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public TimeBucket BusinessStartDate { get; set; }
        public TimeBucket DateNeeded { get; set; }
        public TimeBucket DecisionDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public TimeBucket LastProgressDate { get; set; }
        public TimeBucket StatusDate { get; set; }
        public TimeBucket Submitted { get; set; }
        public Dictionary<string, object> StatusHistory { get; set; }
        public double AnnualRevenue { get; set; }
        public double AverageBankBalance { get; set; }
        public double RequestedAmount { get; set; }
        public double RequestedTermValue { get; set; }
        public IList<string> SocialLinks { get; set; }
        public string ApplicantId { get; set; }
        public string ApplicationNumber { get; set; }
        public string BusinessAddressLine1 { get; set; }
        public string BusinessAddressLine2 { get; set; }
        public string BusinessAddressLine3 { get; set; }
        public string BusinessAddressLine4 { get; set; }
        public string BusinessApplicantName { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessCountry { get; set; }
        public string BusinessCountryCode { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessFax { get; set; }
        public string BusinessLandMark { get; set; }
        public string BusinessLocation { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessState { get; set; }
        public string BusinessTaxID { get; set; }
        public string BusinessType { get; set; }
        public string BusinessWebsite { get; set; }
        public string BusinessZipCode { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string DBA { get; set; }
        public string DUNSNumber { get; set; }
        public string LegalBusinessName { get; set; }
        public string LoanTimeFrame { get; set; }
        public string NAICCode { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string ProductId { get; set; }
        public string PropertyType { get; set; }
        public string PurposeOfLoan { get; set; }
        public string RequestedTermType { get; set; }
        public string SICCode { get; set; }
        public string SourceReferenceId { get; set; }
        public string SourceType { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string TrackingCode { get; set; }
        public string Industry { get; set; }
        public string EIN { get; set; }
        public Dictionary<string, string> Assignees { get; set; }
        public string LeadOwnerUsername { get; set; }
        //new added
        public IList<string> DeclineReason { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        #region DealData
        public double LoanAmount { get; set; }
        public double RepaymentAmount { get; set; }
        public double SellRate { get; set; }
        public double CommissionRate { get; set; }
        public double CommissionAmount { get; set; }
        public string DurationType { get; set; }
        public decimal TermPayment { get; set; }
        public decimal AmountFunded { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string RoutingNumber { get; set; }
        public int Term { get; set; }
        public double ApprovedAmount { get; set; }
        public string TypeOfPayment { get; set; }
        public double PaymentAmount { get; set; }
        public int NumberOfPayment { get; set; }
        public double OriginatingFeeAmount { get; set; }
        public double ACHFee { get; set; }
        public double BuyRate { get; set; }
        public double LenderReturn { get; set; }
        public double NetFundingRate { get; set; }
        public double AfterDefaultAssumption { get; set; }
        public int AverageLife { get; set; }
        public double IRR { get; set; }
        public string Grade { get; set; }
        #endregion
        public string PartnerUserId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string PartnerEmail { get; set; }
        public TimeBucket FundingRequestedDate { get; set; }
        public string FundingReferenceId { get; set; }
        public string FundingRequestStatus { get; set; }
        public TimeBucket FundingReturnDate { get; set; }
        public string FundingReturnCode { get; set; }
        public string FundingReturnReason { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExternalReferences, ExternalReferences>))]
        public IList<IExternalReferences> ExternalReferences { get; set; }
        public string IPAddress { get; set; }
        public IEnumerable<string> Tags { get; set; }
    }
}