﻿using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions.Configurations
{
    public class Configuration
    {
        public EventMapping[] Events { get; set; }
        public Dictionary<string, Rule> Tags { get; set; }
        public string[] ApprovedStatuses { get; set; }

        public string[] ReApplyStatuseCodes { get; set; }
        public int ReApplyTimeFrameDays { get; set; }
        public IEnumerable<ExportTemplateDetails> ExportTemplates { get; set; }
        public string OpenStatusCode { get; set; }
    }
}
