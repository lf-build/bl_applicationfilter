﻿using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions.Configurations
{
    public class TaggingConfiguration
    {
        public List<TaggingEvent> Events { get; set; }
    }
}
