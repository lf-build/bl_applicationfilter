﻿using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions.Configurations
{
    public class ExportTemplateDetails
    {
        public string TemplateName { get; set; }
        public string FileNamePrefix { get; set; }
        public Dictionary<string, string> CsvProjection { get; set; }
        public string Delimeter { get; set; }
    }
}
