﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace CapitalAlliance.Applications.Filters.Abstractions {
    public class Settings {
        public static string ServiceName { get; } = "application-filters";

        public static string TaggingEvents { get; } = "tagging-events";

        private static string Prefix { get; } = ServiceName.ToUpper ();

        public static ServiceSettings EventHub { get; } = new ServiceSettings ($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings ($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings ($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Application { get; } = new ServiceSettings ($"{Prefix}_APPLICATION", "business-application");

        public static ServiceSettings Applicant { get; } = new ServiceSettings ($"{Prefix}_APPLICANT", "business-applicant");

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings ($"{Prefix}_STATUS_MANAGEMENT", "status-management");

        public static ServiceSettings OfferEngine { get; } = new ServiceSettings ($"{Prefix}_OFFER_ENGINE", "offer-engine");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings ($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings ($"{Prefix}_VERIFICATION_ENGINE", "verification-engine");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings ($"{Prefix}_DATA_ATTRIBUTES", "data-attributes");

        public static ServiceSettings ProductRule { get; } = new ServiceSettings ($"{Prefix}_PRODUCTRULE_HOST", "ProductRule", $"{Prefix}_PRODUCTRULE_PORT");
        public static string Nats => Environment.GetEnvironmentVariable ($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings Assignment { get; } = new ServiceSettings ($"{Prefix}_ASSIGNMENT", "assignment");
        public static ServiceSettings Identity { get; } = new ServiceSettings ($"{Prefix}_IDENTITY", "identity");
        public static ServiceSettings Partner { get; } = new ServiceSettings ($"{Prefix}_PARTNER", "partner");
        public static ServiceSettings businessFunding { get; } = new ServiceSettings ($"{Prefix}_BUSINESS-FUNDING", "business-Funding");
    }
}