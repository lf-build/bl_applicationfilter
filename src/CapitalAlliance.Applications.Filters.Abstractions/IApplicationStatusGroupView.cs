﻿namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IApplicationStatusGroupView
    {
        int Count { get; set; }
        string Status { get; set; }
    }
}