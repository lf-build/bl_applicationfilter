﻿using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface ICsvGenerator
    {
        string Delimiter { get; set; }
        byte[] WriteToCsv<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}
