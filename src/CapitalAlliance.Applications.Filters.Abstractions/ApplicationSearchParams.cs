﻿using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationSearchParams
    {
        public int AppliedOnDuration { get; set; }
        public string AppliedOnDurationType { get; set; }
        public int ExpiryOnDuration { get; set; }
        public string ExpiryOnDurationType { get; set; }
        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string BusinessTaxID { get; set; }
        public string BusinessEmail { get; set; }
        public string LegalBusinessName { get; set; }
        public string StatusCode { get; set; }
    }
}