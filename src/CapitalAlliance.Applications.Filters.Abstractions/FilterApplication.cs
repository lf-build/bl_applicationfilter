﻿using LendFoundry.Business.Application;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class FilterApplication : IFilterApplication
    {
        public FilterApplication()
        {
        }
        public IList<string> ApplicationNumber { get; set; }
    }
}