﻿using System;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class TagResult
    {
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
    }
}
