﻿namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }

}
