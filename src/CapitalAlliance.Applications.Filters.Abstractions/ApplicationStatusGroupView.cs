﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class ApplicationStatusGroupView : IApplicationStatusGroupView
    {
        public string Status { get; set; }
        public int Count { get; set; }
    }
}
