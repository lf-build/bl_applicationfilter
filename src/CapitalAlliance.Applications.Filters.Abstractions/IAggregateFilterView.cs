﻿using LendFoundry.Business.Application;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IAggregateFilterView
    {
        bool IsExistingBusinessLoan { get; set; }
        TimeBucket ApplicationDate { get; set; }
        TimeBucket BusinessStartDate { get; set; }
        TimeBucket DateNeeded { get; set; }
        TimeBucket DecisionDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        TimeBucket LastProgressDate { get; set; }
        TimeBucket StatusDate { get; set; }
        TimeBucket Submitted { get; set; }
        Dictionary<string, object> StatusHistory { get; set; }
        double AnnualRevenue { get; set; }
        double AverageBankBalance { get; set; }
        double RequestedAmount { get; set; }
        double RequestedTermValue { get; set; }
        IList<string> SocialLinks { get; set; }
        string ApplicantId { get; set; }
        string ApplicationNumber { get; set; }
        string BusinessAddressLine1 { get; set; }
        string BusinessAddressLine2 { get; set; }
        string BusinessAddressLine3 { get; set; }
        string BusinessAddressLine4 { get; set; }
        string BusinessApplicantName { get; set; }
        string BusinessCity { get; set; }
        string BusinessCountry { get; set; }
        string BusinessCountryCode { get; set; }
        string BusinessEmail { get; set; }
        string BusinessFax { get; set; }
        string BusinessLandMark { get; set; }
        string BusinessLocation { get; set; }
        string BusinessPhone { get; set; }
        string BusinessState { get; set; }
        string BusinessTaxID { get; set; }
        string BusinessType { get; set; }
        string BusinessWebsite { get; set; }
        string BusinessZipCode { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        string DBA { get; set; }
        string DUNSNumber { get; set; }
        string LegalBusinessName { get; set; }
        string LoanTimeFrame { get; set; }
        string NAICCode { get; set; }
        string OtherPurposeDescription { get; set; }
        string ProductId { get; set; }
        string PropertyType { get; set; }
        string PurposeOfLoan { get; set; }
        string RequestedTermType { get; set; }
        string SICCode { get; set; }
        string SourceReferenceId { get; set; }
        string SourceType { get; set; }
        string StatusCode { get; set; }
        string StatusName { get; set; }
        string TrackingCode { get; set; }
        string Industry { get; set; }
        string EIN { get; set; }
        Dictionary<string, string> Assignees { get; set; }
        string LeadOwnerUsername { get; set; }
        //new added
        IList<string> DeclineReason { get; set; }
        IList<IOwner> Owners { get; set; }
        #region DealData
        double LoanAmount { get; set; }
        double RepaymentAmount { get; set; }
        double SellRate { get; set; }
        double CommissionRate { get; set; }
        double CommissionAmount { get; set; }
        string DurationType { get; set; }
        decimal TermPayment { get; set; }
        decimal AmountFunded { get; set; }
        string BankName { get; set; }
        string AccountNumber { get; set; }
        string AccountType { get; set; }
        string RoutingNumber { get; set; }
        int Term { get; set; }
        //added fields
        double ApprovedAmount { get; set; }
        string TypeOfPayment { get; set; }
        double PaymentAmount { get; set; }
        int NumberOfPayment { get; set; }
        double OriginatingFeeAmount { get; set; }
        double ACHFee { get; set; }
        double BuyRate { get; set; }
        double LenderReturn { get; set; }
        double NetFundingRate { get; set; }
        double AfterDefaultAssumption { get; set; }
        int AverageLife { get; set; }
        double IRR { get; set; }
        string Grade { get; set; }
        #endregion
        string PartnerUserId { get; set; }
        string PartnerId { get; set; }
        string PartnerName { get; set; }
        string PartnerEmail { get; set; }

        TimeBucket FundingRequestedDate { get; set; }
        string FundingReferenceId { get; set; }
        string FundingRequestStatus { get; set; }
        TimeBucket FundingReturnDate { get; set; }
        string FundingReturnCode { get; set; }
        string FundingReturnReason { get; set; }
        IList<IExternalReferences> ExternalReferences { get; set; }
        string IPAddress { get; set; }
        IEnumerable<string> Tags { get; set; }
    }
}
