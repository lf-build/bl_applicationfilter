﻿namespace CapitalAlliance.Applications.Filters {
    public interface IOwner {
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
        string DOB { get; set; }
        string OwnershipType { get; set; }
        string OwnershipPercentage { get; set; }
        string Designation { get; set; }
        string EmailAddress { get; set; }
        string PhoneNumber { get; set; }

        string MobileNumber { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressLine3 { get; set; }
        string AddressLine4 { get; set; }
        string LandMark { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Country { get; set; }
        bool? IsPrimary { get; set; }
        string OwnerId { get; set; }
    }
}