﻿using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewTagsRepositoryFactory
    {
        IFilterViewTagsRepository Create(ITokenReader reader);
    }
}
