﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityId);
        IEnumerable<IFilterView> GetAll();

        void AddOrUpdate(IFilterView view);

        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, TimeBucket currentMonth);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, TimeBucket currentYear);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);


        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);


        Task<IFilterView> GetUsingUniqueAttributes(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(TimeBucket todayDate);
        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(TimeBucket todayDate, List<string> excludedStatuses);

        IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null);
        Task<IEnumerable<IFilterView>> GetAllUnassigned();

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName);

        Task<IEnumerable<IFilterView>> GetAllUnassignedByUser(IEnumerable<string> roles);

        Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(string partnerId = null);

        Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(DateTimeOffset fromDate, DateTimeOffset toDate, string partnerId = null);
        Task<IFilterView> GetByApplicationNumber(string applicationNumber);
    }
}