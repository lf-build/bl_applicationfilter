﻿//using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

#if DOTNET2

using Microsoft.AspNetCore.Mvc;

#else

//using Microsoft.AspNet.Mvc;

#endif

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterService
    {
        Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityIds);

        IEnumerable<IFilterView> GetAll();

        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        #region Tagging realated methods

        Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags);

        Task<int> GetCountByTag(IEnumerable<string> tags);

        Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag);

        #endregion Tagging realated methods

        double GetTotalAmountApprovedBySource(string sourceType, string sourceId);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber, string productId);

        Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications();

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(List<string> excludedStatuses);

        void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);

        void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);

        IFilterViewTags GetApplicationTagInformation(string applicationNumber);

        IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null);

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications();

        Task<IEnumerable<IFilterView>> GetAllUnassigned();

        Task<IEnumerable<IFilterView>> GetAllUnassignedByUser();

        FileContentResult ExportAll(string templateName);

        Task<FileContentResult> ExportAllByStatus(string templateName, IEnumerable<string> statuses);

        Task<FileContentResult> ExportAllByTag(string templateName, IEnumerable<string> tags);

        Task<List<IStatusHistory>> GetStatusHistoryWithUser(bool isThumbnailInclude, string applicationNumber, string productId);

        Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus();

        Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus(int timeSpan);

        Task<IEnumerable<IApplicationStatusGroupView>> GetPartnerApplicationCountByStatus(string partnerId);

        Task<IEnumerable<IApplicationStatusGroupView>> GetPartnerApplicationCountByStatus(int timeSpan, string partnerId);

        Task<IAggregateFilterView> GetByApplicationNumberWithTags(string applicationNumber);
    }
}