﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewTagsRepository : IRepository<IFilterViewTags>
    {
        Task<IEnumerable<IFilterViewTags>> GetByTags(IEnumerable<string> tags);
        Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag);
        Task<int> GetCountByTag(IEnumerable<string> tags);
        void AddTag(string applicationNumber, string tag);
        void RemoveTag(string applicationNumber, string tag);
        void ClearAllTags(string applicationNumber);
        Task<IEnumerable<IFilterViewTags>> GetByNotInTags(IEnumerable<string> tags);
        IFilterViewTags GetTagsForApplication(string applicationNumber);
    }
}