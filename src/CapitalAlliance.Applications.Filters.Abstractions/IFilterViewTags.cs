﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IFilterViewTags : IAggregate
    {
        string ApplicationNumber { get; set; }
        IEnumerable<TagInfo> Tags { get; set; }
    }
}
