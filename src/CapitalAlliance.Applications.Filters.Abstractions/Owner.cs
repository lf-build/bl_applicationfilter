﻿namespace CapitalAlliance.Applications.Filters {
    public class Owner : IOwner {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string DOB { get; set; }
        public string OwnershipType { get; set; }
        public string OwnershipPercentage { get; set; }
        public string Designation { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string LandMark { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public bool? IsPrimary { get; set; }
        public string OwnerId { get; set; }
    }
}