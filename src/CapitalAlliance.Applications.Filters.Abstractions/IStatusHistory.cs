﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IStatusHistory
    {
         string ActivatedBy { get; set; }
         string Status { get; set; }
         TimeBucket ActiveOn { get; set; }
         string ThumbNailImage { get; set; }
         List<string> Reason { get; set; }

    }
}
