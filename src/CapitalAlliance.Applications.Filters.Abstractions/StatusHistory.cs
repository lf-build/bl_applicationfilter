﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class StatusHistory: IStatusHistory
    {
        public string ActivatedBy { get; set; }
        public string Status { get; set; }
        public TimeBucket ActiveOn { get; set; }
        public string ThumbNailImage { get; set; }
        public List<string> Reason { get; set; }

    }
}
