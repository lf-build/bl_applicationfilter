﻿namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public enum UniqueParameterTypes
    {
        Pan,
        Email,
        Mobile
    }
}
