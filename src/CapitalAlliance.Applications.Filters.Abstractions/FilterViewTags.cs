﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class FilterViewTags : Aggregate, IFilterViewTags, IEquatable<FilterViewTags>
    {
        public FilterViewTags(string applicationNumber, IEnumerable<TagInfo> tags)
        {
            ApplicationNumber = applicationNumber;
            Tags = tags;
        }
        public string ApplicationNumber { get; set; }
        
        public IEnumerable<TagInfo> Tags { get; set; }

        public bool Equals(FilterViewTags other)
        {
            return other == null ? false : this.ApplicationNumber == other.ApplicationNumber && this.TenantId == other.TenantId;
        }
    }
}
