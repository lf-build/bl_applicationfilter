﻿namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public enum DurationType
    {
        Day = 1,
        Week = 2,
        Month = 3,
        Year = 4
    }
}
