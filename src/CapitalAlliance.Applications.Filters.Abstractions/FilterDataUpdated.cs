﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class FilterDataUpdated
    {
        public string EventName { get; set; }

        public IFilterView Data { get; set; }
    }
}
