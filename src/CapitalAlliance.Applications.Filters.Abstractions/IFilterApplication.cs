﻿using LendFoundry.Business.Application;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IFilterApplication
    {
        IList<string> ApplicationNumber { get; set; }
    }
}