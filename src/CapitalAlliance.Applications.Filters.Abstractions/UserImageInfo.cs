﻿using System;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class UserImageInfo
    {
        public string UserName { get; set; }
        public string ThumbNail { get; set; }
    }
}
