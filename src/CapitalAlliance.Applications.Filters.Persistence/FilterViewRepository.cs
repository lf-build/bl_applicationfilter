﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement;
using LendFoundry.Business.Application;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CapitalAlliance.Applications.Filters.Persistence {
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository {
        static FilterViewRepository () {

            BsonClassMap.RegisterClassMap<TimeBucket> (map => {
                map.AutoMap ();
                map.MapMember (m => m.Time).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));
                var type = typeof (TimeBucket);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (false);
            });
            BsonClassMap.RegisterClassMap<ExternalReferences> (map => {
                map.AutoMap ();
                var type = typeof (ExternalReferences);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<SubStatus> (map => {
                map.AutoMap ();
                var type = typeof (SubStatus);
                map.MapProperty (p => p.SubWorkFlowStatus).SetSerializer (new EnumSerializer<WorkFlowStatus> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<StatusWorkFlow> (map => {
                map.AutoMap ();
                var type = typeof (StatusWorkFlow);
                map.MapProperty (p => p.WorkFlowStatus).SetSerializer (new EnumSerializer<WorkFlowStatus> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }
        public FilterViewRepository (ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService) : base (tenantService, configuration, "application-filters") {
            if (!BsonClassMap.IsClassMapRegistered (typeof (FilterView))) {
                BsonClassMap.RegisterClassMap<FilterView> (map => {
                    map.AutoMap ();
                    map.MapProperty (a => a.Owners).SetIgnoreIfDefault (true);
                    map.MapMember (m => m.Assignees)
                        .SetSerializer (new DictionaryInterfaceImplementerSerializer<Dictionary<string, string>>
                            (
                                DictionaryRepresentation.ArrayOfDocuments
                            )
                        );
                    map.MapMember (m => m.BusinessTaxID).SetSerializer (new BsonEncryptor<string, string> (encrypterService));
                    var type = typeof (FilterView);
                    map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass (true);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered (typeof (Owner))) {
                BsonClassMap.RegisterClassMap<Owner> (map => {
                    map.AutoMap ();
                    map.MapMember (m => m.SSN).SetSerializer (new BsonEncryptor<string, string> (encrypterService));
                    map.MapMember (m => m.DOB).SetSerializer (new BsonEncryptor<string, string> (encrypterService));
                    var type = typeof (Owner);
                    map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }
            var namePerEntity = Builders<IFilterView>.IndexKeys
                .Ascending (a => a.TenantId)
                .Ascending (a => a.ApplicationNumber);
            CreateIndexIfNotExists (nameof (namePerEntity), namePerEntity, true);
            CreateIndexIfNotExists ("entityType_entityID", Builders<IFilterView>.IndexKeys.Ascending (a => a.TenantId).Ascending (a => a.ApplicationNumber));
            CreateIndexIfNotExists ("business-applicant-name", Builders<IFilterView>.IndexKeys.Ascending (i => i.BusinessApplicantName));
            CreateIndexIfNotExists ("status-code", Builders<IFilterView>.IndexKeys.Ascending (i => i.StatusCode));
            CreateIndexIfNotExists ("status-name", Builders<IFilterView>.IndexKeys.Ascending (i => i.StatusName));
        }

        public void AddOrUpdate (IFilterView view) {
            if (view == null)
                throw new ArgumentNullException (nameof (view));

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.ApplicationNumber == view.ApplicationNumber;

            var existingView = Query
                .Where (v => v.ApplicationNumber == view.ApplicationNumber && v.TenantId == TenantService.Current.Id)
                .FirstOrDefault ();

            view.TenantId = TenantService.Current.Id;
            if (existingView != null && !string.IsNullOrEmpty (existingView.Id))
                view.Id = existingView.Id;
            else
                view.Id = ObjectId.GenerateNewId ().ToString ();

            Collection.FindOneAndReplace (query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
        }

        public IEnumerable<IFilterView> GetAll () {
            return OrderIt (Query);
        }

        public IEnumerable<IFilterView> GetByStatus (IEnumerable<string> statuses) {

            return OrderIt (Query.Where (x => statuses.Contains (x.StatusCode)));
        }

        public IEnumerable<IFilterView> GetAllBySource (string sourceType, string sourceId) {
            return OrderIt (Query.Where (x => x.SourceType.ToLower () == sourceType.ToLower () && x.SourceReferenceId == sourceId));
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus (string sourceType, string sourceId, IEnumerable<string> statuses) {
            return OrderIt (Query.Where (x => x.SourceType.ToLower () == sourceType.ToLower () && x.SourceReferenceId == sourceId && statuses.Contains (x.StatusCode)));
        }

        private IQueryable<IFilterView> OrderIt (IEnumerable<IFilterView> filterView) {
            return filterView.OrderBy (x => x.StatusCode)
                .ThenBy (x => x.Submitted).AsQueryable ();
        }

        public Task<int> GetTotalSubmittedForCurrentMonth (string sourceType, string sourceId, TimeBucket currentMonth) {
            return Task.FromResult (Query.Count (
                app => app.SourceType.ToLower () == sourceType.ToLower () &&
                app.SourceReferenceId == sourceId &&
                app.Submitted.Time >= currentMonth.Time
            ));
        }

        public Task<int> GetTotalSubmittedForCurrentYear (string sourceType, string sourceId, TimeBucket currentYear) {
            return Task.FromResult (Query.Count (
                app => app.SourceType.ToLower () == sourceType.ToLower () &&
                app.SourceReferenceId == sourceId &&
                app.Submitted.Time >= currentYear.Time
            ));
        }

        public Task<IEnumerable<IFilterView>> GetByName (string sourceType, string sourceId, string name) {
            return Task.FromResult<IEnumerable<IFilterView>> (
                OrderIt (
                    Query.Where (a => a.SourceType.ToLower () == sourceType.ToLower () &&
                        a.SourceReferenceId == sourceId &&
                        a.BusinessApplicantName.ToLower ().Contains (name.ToLower ()))));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus (string sourceType, string sourceId, IEnumerable<string> statuses) {
            return Task.FromResult<IEnumerable<IFilterView>>
                (
                    OrderIt (Query.Where (x => x.SourceType.ToLower () == sourceType.ToLower () &&
                        x.SourceReferenceId == sourceId &&
                        statuses.Contains (x.StatusCode)))
                );
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus (IEnumerable<string> statuses) {
            return Task.FromResult<IEnumerable<IFilterView>>
                (
                    OrderIt (Query.Where (x => statuses.Contains (x.StatusCode)))
                );
        }

        public Task<int> GetCountByStatus (string sourceType, string sourceId, IEnumerable<string> statuses) {
            return Task.FromResult<int>
                (
                    Query.Count (x => x.SourceType.ToLower () == sourceType.ToLower () &&
                        x.SourceReferenceId == sourceId &&
                        statuses.Contains (x.StatusCode))
                );
        }

        public Task<IFilterView> GetByApplicationNumber (string sourceType, string sourceId, string applicationNumber) {
            return Task.FromResult (
                Query.FirstOrDefault (a => a.SourceType.ToLower () == sourceType.ToLower () &&
                    a.SourceReferenceId == sourceId &&
                    a.ApplicationNumber == applicationNumber)
            );
        }

        public Task<IFilterView> GetUsingUniqueAttributes (UniqueParameterTypes parameterType, string parameterValue) {
            Task<IFilterView> returnVal = null;
            switch (parameterType) {
                case UniqueParameterTypes.Email:
                    returnVal = Task.FromResult (Query.OrderByDescending (x => x.Submitted)?.FirstOrDefault (a => a.BusinessEmail.ToLower () == parameterValue.ToLower ()));
                    break;

                case UniqueParameterTypes.Mobile:
                    returnVal = Task.FromResult (Query.OrderByDescending (x => x.Submitted)?.FirstOrDefault (a => a.BusinessPhone == parameterValue));
                    break;

                    //case UniqueParameterTypes.Pan:
                    //    returnVal = Task.FromResult(Query.OrderByDescending(x => x.Submitted)?.FirstOrDefault(a => a.ApplicantPanNumber.ToUpper() == parameterValue.ToUpper()));
                    //    break;
            }
            return returnVal;
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications (TimeBucket todayDate) {
            return await Task.FromResult (
                Query.Where (p => p.ExpiryDate.Time < todayDate.Time).ToList ()
            );
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications (TimeBucket todayDate, List<string> excludedStatuses) {
            return await Task.FromResult (
                Query.Where (p => p.ExpiryDate.Time < todayDate.Time && !excludedStatuses.Contains (p.StatusCode)).ToList ()
            );
        }

        public async Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds (IFilterApplication entityIds) {

            return await Task.FromResult (
                Query.Where (p => entityIds.ApplicationNumber.Contains (p.ApplicationNumber)).ToList ()
            );
        }
        private void GetDateFilters (int duration, DurationType durationType, bool IsFutureDate, ref DateTime fromDate, ref DateTime toDate) {
            int factor = IsFutureDate ? 1 : -1;

            DateTime tempDate = DateTime.UtcNow;
            fromDate = tempDate;
            toDate = tempDate;

            switch (durationType) {
                case DurationType.Day:
                    tempDate = DateTime.UtcNow.AddDays (factor * duration);
                    break;
                case DurationType.Week:
                    tempDate = DateTime.UtcNow.AddDays (7 * factor * duration);
                    break;
                case DurationType.Month:
                    tempDate = DateTime.UtcNow.AddMonths (factor * duration);
                    break;
                case DurationType.Year:
                    tempDate = DateTime.UtcNow.AddYears (factor * duration);
                    break;
            }

            if (IsFutureDate) {
                toDate = tempDate;
            } else {
                fromDate = tempDate;
            }
        }
        // public async Task<IEnumerable<IFilterView>> GetApplicationsByEntityIds(IFilterApplication entityIds)
        // {
        //     return await Task.FromResult
        //     (
        //        Query.Where(p => entityIds.ApplicationNumber.Contains(p.ApplicationNumber)).ToList()
        //     );
        // }

        #region Search Application

        public IEnumerable<IFilterView> SearchApplications (ApplicationSearchParams searchParams, string condition = null) {
            var filters = new List<FilterDefinition<IFilterView>> ();
            var fromDate = DateTime.UtcNow;
            var toDate = DateTime.UtcNow;
            if (searchParams.AppliedOnDuration > 0 && !string.IsNullOrEmpty (searchParams.AppliedOnDurationType)) {
                GetDateFilters (searchParams.AppliedOnDuration, (DurationType) Enum.Parse (typeof (DurationType), searchParams.AppliedOnDurationType), false, ref fromDate, ref toDate);
                var fromExpirationFilter = Builders<IFilterView>.Filter.Gte ("Submitted.Ticks", fromDate.Ticks);
                var toExpirationFilter = Builders<IFilterView>.Filter.Lte ("Submitted.Ticks", toDate.Ticks);
                var appFilter = Builders<IFilterView>.Filter.And (fromExpirationFilter, toExpirationFilter);

                filters.Add (appFilter);
            }

            if (searchParams.ExpiryOnDuration > 0 && !string.IsNullOrEmpty (searchParams.ExpiryOnDurationType)) {
                GetDateFilters (searchParams.ExpiryOnDuration, (DurationType) Enum.Parse (typeof (DurationType), searchParams.ExpiryOnDurationType), true, ref fromDate, ref toDate);

                var fromExpirationFilter = Builders<IFilterView>.Filter.Gte ("ExpiryDate.0", fromDate.Ticks);
                var toExpirationFilter = Builders<IFilterView>.Filter.Lte ("ExpiryDate.0", toDate.Ticks);
                var appFilter = Builders<IFilterView>.Filter.And (fromExpirationFilter, toExpirationFilter);
                filters.Add (appFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.MobileNumber)) {
                var mobileFilter = Builders<IFilterView>.Filter.Where (x => x.BusinessPhone == searchParams.MobileNumber);
                filters.Add (mobileFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.Name)) {
                searchParams.Name = searchParams.Name.ToLower ();
                var nameFilter = Builders<IFilterView>.Filter.Where (x => x.ContactFirstName.ToLower ().Contains (searchParams.Name) || x.ContactLastName.ToLower ().Contains (searchParams.Name));
                filters.Add (nameFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.BusinessTaxID)) {
                var businessTaxIDFilter = Builders<IFilterView>.Filter.Where (x => x.BusinessTaxID == searchParams.BusinessTaxID);
                filters.Add (businessTaxIDFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.BusinessEmail)) {
                var emailFilter = Builders<IFilterView>.Filter.Where (x => x.BusinessEmail == searchParams.BusinessEmail);
                filters.Add (emailFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.LegalBusinessName)) {
                searchParams.LegalBusinessName = searchParams.LegalBusinessName.ToLower ();
                var legalBusinessNameFilter = Builders<IFilterView>.Filter.Where (x => x.LegalBusinessName.ToLower ().Contains (searchParams.LegalBusinessName));
                filters.Add (legalBusinessNameFilter);
            }

            if (!string.IsNullOrEmpty (searchParams.StatusCode)) {
                var statusFilter = Builders<IFilterView>.Filter.Where (x => x.StatusCode == searchParams.StatusCode);
                filters.Add (statusFilter);
            }

            if (filters.Count > 0) {
                FilterDefinition<IFilterView> finalFilter = null;
                if (!string.IsNullOrEmpty (condition) && condition.Equals ("and", StringComparison.OrdinalIgnoreCase)) {
                    filters.Add (Builders<IFilterView>.Filter.Where (x => x.TenantId == TenantService.Current.Id));
                    finalFilter = Builders<IFilterView>.Filter.And (filters);
                    return Collection.Find (finalFilter).ToEnumerable<IFilterView> ();
                } else {
                    finalFilter = Builders<IFilterView>.Filter.Or (filters);
                    return Collection.Find (finalFilter).ToEnumerable<IFilterView> ().Where (x => x.TenantId == TenantService.Current.Id);
                }
            } else {
                return null;
            }
        }

        #endregion
        #region assignment filters
        public async Task<IEnumerable<IFilterView>> GetAllUnassigned () {
            return await Query.Where (i => i.Assignees == null).ToListAsync ();

        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications (string userName) {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq (db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn ("Assignees.v", new List<string> () { userName });
            return await Collection.Find (builders.And (filterByTenantId, filterByUserName)).ToListAsync ();
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassignedByUser (IEnumerable<string> roles) {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq (db => db.TenantId, tenantId);
            var filterNotInRoles = builders.AnyNin ("Assignees.k", roles);
            var result = await Collection.Find (builders.And (filterByTenantId, filterNotInRoles)).ToListAsync ();

            return result.ToList ();
        }
        #endregion

        #region Metric Collector Agrregate Quries

        public async Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus (string partnerId = null) {
            return await Task.Run (() => {
                if (!string.IsNullOrWhiteSpace (partnerId)) {
                    var aggregate = Collection.Aggregate ().Match (new BsonDocument { { "PartnerId", partnerId } }).Match (new BsonDocument { { "TenantId", TenantService.Current.Id } }).Group (new BsonDocument { { "_id", "$StatusCode" }, { "count", new BsonDocument ("$sum", 1) } });
                    List<IApplicationStatusGroupView> returnResult = aggregate.ToList ().Select (x => new ApplicationStatusGroupView () { Status = x["_id"].ToString (), Count = (int) x["count"] }).ToList<IApplicationStatusGroupView> ();
                    return returnResult;
                } else {
                    var aggregate = Collection.Aggregate ().Match (new BsonDocument { { "TenantId", TenantService.Current.Id } }).Group (new BsonDocument { { "_id", "$StatusCode" }, { "count", new BsonDocument ("$sum", 1) } });
                    List<IApplicationStatusGroupView> returnResult = aggregate.ToList ().Select (x => new ApplicationStatusGroupView () { Status = x["_id"].ToString (), Count = (int) x["count"] }).ToList<IApplicationStatusGroupView> ();
                    return returnResult;
                }
            });
        }
        public async Task<IEnumerable<IApplicationStatusGroupView>> GetApplicationCountByStatus (DateTimeOffset fromDate, DateTimeOffset toDate, string partnerId = null) {
            return await Task.Run (() => {
                if (!string.IsNullOrWhiteSpace (partnerId)) {
                    var aggregate = Collection.Aggregate ().Match (r => r.ApplicationDate.Time >= fromDate.UtcDateTime && r.ApplicationDate.Time <= toDate.UtcDateTime && r.PartnerId == partnerId && r.TenantId == TenantService.Current.Id).Group (new BsonDocument { { "_id", "$StatusCode" }, { "count", new BsonDocument ("$sum", 1) } });
                    List<IApplicationStatusGroupView> returnResult = aggregate.ToList ().Select (x => new ApplicationStatusGroupView () { Status = x["_id"].ToString (), Count = (int) x["count"] }).ToList<IApplicationStatusGroupView> ();
                    return returnResult;
                } else {
                    var aggregate = Collection.Aggregate ().Match (r => r.ApplicationDate.Time >= fromDate.UtcDateTime && r.ApplicationDate.Time <= toDate.UtcDateTime && r.TenantId == TenantService.Current.Id).Group (new BsonDocument { { "_id", "$StatusCode" }, { "count", new BsonDocument ("$sum", 1) } });
                    List<IApplicationStatusGroupView> returnResult = aggregate.ToList ().Select (x => new ApplicationStatusGroupView () { Status = x["_id"].ToString (), Count = (int) x["count"] }).ToList<IApplicationStatusGroupView> ();
                    return returnResult;
                }
            });
        }
        #endregion

        public async Task<IFilterView> GetByApplicationNumber (string applicationNumber) {
            return await Task.FromResult (
                Query.FirstOrDefault (a => a.ApplicationNumber == applicationNumber)
            );
        }
    }
}