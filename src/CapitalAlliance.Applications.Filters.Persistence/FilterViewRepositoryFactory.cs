﻿using System;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CapitalAlliance.Applications.Filters.Persistence {
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory {
        public FilterViewRepositoryFactory (IServiceProvider provider) {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create (ITokenReader reader) {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory> ();
            var tenantService = tenantServiceFactory.Create (reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration> ();
            var encryptionService = Provider.GetService<IEncryptionService> ();
            return new FilterViewRepository (tenantService, mongoConfiguration, encryptionService);
        }
    }
}