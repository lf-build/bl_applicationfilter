﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

//using Microsoft.Framework.DependencyInjection;
using CapitalAlliance.Applications.Filters.Abstractions.Services;

namespace CapitalAlliance.Applications.Filters.Persistence {
    public class FilterViewTagsRepositoryFactory : IFilterViewTagsRepositoryFactory {
        public FilterViewTagsRepositoryFactory (IServiceProvider provider) {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewTagsRepository Create (ITokenReader reader) {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory> ();
            var tenantService = tenantServiceFactory.Create (reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration> ();
            return new FilterViewTagsRepository (tenantService, mongoConfiguration);
        }
    }
}