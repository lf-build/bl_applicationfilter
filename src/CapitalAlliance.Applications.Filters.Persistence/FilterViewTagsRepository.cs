﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Persistence
{
    public class FilterViewTagsRepository : MongoRepository<IFilterViewTags, FilterViewTags>, IFilterViewTagsRepository
    {
        static FilterViewTagsRepository()
        {
            BsonClassMap.RegisterClassMap<FilterViewTags>(map =>
            {
                map.AutoMap();
                var type = typeof(FilterViewTags);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public FilterViewTagsRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "application-filters-tags")
        {
            CreateIndexIfNotExists("application", Builders<IFilterViewTags>.IndexKeys.Ascending(i => i.ApplicationNumber));
        }
        public async Task<IEnumerable<IFilterViewTags>> GetByTags(IEnumerable<string> tags)
        {
            List<FilterDefinition<IFilterViewTags>> filters = GetFilter(null, tags);
            var finalFilter = Builders<IFilterViewTags>.Filter.And(filters);
            var result = await Collection.Find(finalFilter).ToListAsync();
            return result;
        }

        public Task<IEnumerable<IFilterViewTags>> GetByNotInTags(IEnumerable<string> tags)
        {
            var filters = new List<FilterDefinition<IFilterViewTags>>();
            filters.Add(Builders<IFilterViewTags>.Filter.Eq("TenantId", TenantService.Current.Id));
            filters.Add(Builders<IFilterViewTags>.Filter.Nin("Tags.TagName", tags));

            var finalFilter = Builders<IFilterViewTags>.Filter.And(filters);
            return Task.FromResult<IEnumerable<IFilterViewTags>>
            (
                Collection.Find(finalFilter).ToEnumerable<IFilterViewTags>()
            );
        }

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            List<FilterDefinition<IFilterViewTags>> filters = GetFilter(tag, null);
            var finalFilter = Builders<IFilterViewTags>.Filter.And(filters);
            var result = await Collection.Find(finalFilter).ToListAsync();
            return result;
        }
        public async Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            List<FilterDefinition<IFilterViewTags>> filters = GetFilter(null, tags);
            var finalFilter = Builders<IFilterViewTags>.Filter.And(filters);
            var result = await Collection.Find(finalFilter).CountAsync();
            return Convert.ToInt32(result);
        }
        public IFilterViewTags GetTagsForApplication(string applicationNumber)
        {
            return Query.SingleOrDefault(x => x.ApplicationNumber == applicationNumber);
        }
        public void AddTag(string applicationNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;
            var tagRecord = Query.FirstOrDefault(x => x.ApplicationNumber == applicationNumber);
            var tags = tagRecord?.Tags.Select(x => x.TagName).ToList();
            bool addTag = true;
            if (tags != null)
            {
                if (tags.Contains(tag))
                {
                    addTag = false;
                }
            }

            if (addTag == true)
            {
                Collection.UpdateOne(s =>
                        s.TenantId == tenantId &&
                        s.ApplicationNumber == applicationNumber,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                            .AddToSet<TagInfo>(a => a.Tags, new TagInfo() { TagName = tag, TaggedOn = new TimeBucket(DateTimeOffset.Now) })
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        public void RemoveTag(string applicationNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;

            var tagRecord = Query.FirstOrDefault(x => x.ApplicationNumber == applicationNumber);
            var tagInfo = tagRecord?.Tags?.FirstOrDefault(x => x.TagName == tag);

            if (tagInfo != null)
            {
                Collection.UpdateOne(s =>
                        s.TenantId == tenantId &&
                        s.ApplicationNumber == applicationNumber,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                        .Pull<TagInfo>(a => a.Tags, tagInfo)
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        public void ClearAllTags(string applicationNumber)
        {
            var tenantId = TenantService.Current.Id;
            var tagRecord = Query.FirstOrDefault(x => x.ApplicationNumber == applicationNumber);
            var tags = tagRecord.Tags;

            if (tagRecord != null)
            {
                Collection.UpdateOne(s =>
                        s.TenantId == tenantId &&
                        s.ApplicationNumber == applicationNumber,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                        .PullAll<TagInfo>(a => a.Tags, tags)
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        private IQueryable<IFilterViewTags> OrderIt(IEnumerable<IFilterViewTags> filterTagsView)
        {
            var tenantId = TenantService.Current.Id;
            return filterTagsView.TakeWhile(x => x.TenantId.ToLower() == tenantId.ToLower()).OrderBy(x => x.ApplicationNumber).AsQueryable();
        }

        private List<FilterDefinition<IFilterViewTags>> GetFilter(string tag, IEnumerable<string> tags)
        {
            var filters = new List<FilterDefinition<IFilterViewTags>>();
            filters.Add(Builders<IFilterViewTags>.Filter.Eq("TenantId", TenantService.Current.Id));
            if (!string.IsNullOrEmpty(tag))
                filters.Add(Builders<IFilterViewTags>.Filter.ElemMatch(x => x.Tags, x => x.TagName == tag));
            if (tags != null)
                filters.Add(Builders<IFilterViewTags>.Filter.ElemMatch(x => x.Tags, x => tags.Contains(x.TagName)));
            return filters;
        }
    }
}
