﻿#if DOTNET2
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

#else
using Microsoft.AspNet.Mvc;
#endif

namespace CapitalAlliance.Applications.Filters.Api.Controllers {
    [Route ("/")]
    public class FilterController : ExtendedController {
        public FilterController (IApplicationFilterService service) {
            Service = service;
        }

        private IApplicationFilterService Service { get; }

        [HttpGet ("/all")]
#if DOTNET2
        [ProducesResponseType (typeof (IFilterView), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
#endif
        public IActionResult GetAll () {
            return Execute (() => Ok (Service.GetAll ()));
        }

        [HttpGet ("/{sourceType}/{sourceId}/all")]
        public IActionResult GetAllBySource (string sourceType, string sourceId) {
            return Execute (() => Ok (Service.GetAllBySource (sourceType, sourceId)));
        }

        [HttpGet ("/{sourceType}/{sourceId}/metrics/submit/month")]
        public Task<IActionResult> GetTotalSubmittedForCurrentMonth (string sourceType, string sourceId) {
            return ExecuteAsync (async () => Ok (await Service.GetTotalSubmittedForCurrentMonth (sourceType, sourceId)));
        }

        [HttpGet ("/{sourceType}/{sourceId}/metrics/submit/year")]
        public Task<IActionResult> GetTotalSubmittedForCurrentYear (string sourceType, string sourceId) {
            return ExecuteAsync (async () => Ok (await Service.GetTotalSubmittedForCurrentYear (sourceType, sourceId)));
        }

        [HttpGet ("/{sourceType}/{sourceId}/status/{*statuses}")]
        public Task<IActionResult> GetByStatus (string sourceType, string sourceId, string statuses) {
            return ExecuteAsync (async () => {
                statuses = WebUtility.UrlDecode (statuses);
                var listStatus = statuses?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok (await Service.GetByStatus (sourceType, sourceId, listStatus));
            });
        }

        [HttpGet ("status/{*statuses}")]
        public Task<IActionResult> GetAllByStatus (string statuses) {
            return ExecuteAsync (async () => {
                statuses = WebUtility.UrlDecode (statuses);
                var listStatus = statuses?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok (await Service.GetAllByStatus (listStatus));
            });
        }

        [HttpGet ("/{sourceType}/{sourceId}/count/status/{*statuses}")]
        public Task<IActionResult> GetCountByStatus (string sourceType, string sourceId, string statuses) {
            return ExecuteAsync (async () => {
                statuses = WebUtility.UrlDecode (statuses);
                var listStatus = statuses?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok (await Service.GetCountByStatus (sourceType, sourceId, listStatus));
            });
        }

        #region Tag related code

        [HttpGet ("tag/{*tags}")]
        public Task<IActionResult> GetAllByTag (string tags) {
            return ExecuteAsync (async () => {
                tags = WebUtility.UrlDecode (tags);
                var listTags = tags?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok (await Service.GetAllByTag (listTags));
            });
        }

        [HttpGet ("/count/tag/{*tags}")]
        public Task<IActionResult> GetCountByTag (string tags) {
            return ExecuteAsync (async () => {
                tags = WebUtility.UrlDecode (tags);
                var listTags = tags?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok (await Service.GetCountByTag (listTags));
            });
        }

        [HttpGet ("tag/taginfo/{tag}")]
        public Task<IActionResult> GetTagInfoForApplicationsByTag (string tag) {
            return ExecuteAsync (async () => {
                return Ok (await Service.GetTagInfoForApplicationsByTag (WebUtility.UrlDecode (tag)));
            });
        }

        [HttpPost ("applytags/{applicationnumber}/{*tags}")]
        public IActionResult ApplyTagsToApplication (string applicationnumber, string tags) {
            try {
                return Execute (() => {
                    tags = WebUtility.UrlDecode (tags);
                    var listTags = tags?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.TagsWithoutEvaluation (applicationnumber, listTags);
                    return Ok ();
                });
            } catch (ArgumentNullException ex) {
                return new ErrorResult (404, ex.Message);
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        [HttpPost ("removetags/{applicationnumber}/{*tags}")]
        public IActionResult RemoveTagsFromApplication (string applicationnumber, string tags) {
            try {
                return Execute (() => {
                    tags = WebUtility.UrlDecode (tags);
                    var listTags = tags?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.UnTagWithoutEvaluation (applicationnumber, listTags);
                    return Ok ();
                });
            } catch (ArgumentNullException ex) {
                return new ErrorResult (404, ex.Message);
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        [HttpGet ("getapplicationtaginfo/{applicationnumber}")]
        public IActionResult GetApplicationTagInfo (string applicationnumber) {
            try {
                return Execute (() => {
                    return Ok (Service.GetApplicationTagInformation (applicationnumber));
                });
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        #endregion Tag related code

        [HttpGet ("/{sourceType}/{sourceId}/search/{name}")]
        public Task<IActionResult> GetByName (string sourceType, string sourceId, string name) {
            return ExecuteAsync (async () => {
                name = WebUtility.UrlDecode (name);
                return Ok (await Service.GetByName (sourceType, sourceId, name));
            });
        }

        [HttpGet ("/{sourceType}/{sourceId}/metrics/total-approved-amount")]
        public IActionResult GetTotalAmountApprovedBySource (string sourceType, string sourceId) {
            return Execute (() => Ok (Service.GetTotalAmountApprovedBySource (sourceType, sourceId)));
        }

        [HttpGet ("/{sourceType}/{sourceId}/{applicationNumber}")]
        public Task<IActionResult> GetByApplicationNumber (string sourceType, string sourceId, string applicationNumber) {
            return ExecuteAsync (async () => Ok (await Service.GetByApplicationNumber (sourceType, sourceId, applicationNumber)));
        }

        [HttpGet ("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}/{productId?}")]
        public Task<IActionResult> GetByApplicationNumberWithStatusHistory (string sourceType, string sourceId, string applicationNumber, string productId) {
            return ExecuteAsync (async () => Ok (await Service.GetByApplicationNumberWithStatusHistory (sourceType, sourceId, applicationNumber, productId)));
        }

        [HttpGet ("getstatushistory/{applicationNumber}/{isThumbnailInclude}/{productId?}")]
        public Task<IActionResult> GetStatusHistoryWithUser (bool isThumbnailInclude, string applicationNumber, string productId) {
            return ExecuteAsync (async () => Ok (await Service.GetStatusHistoryWithUser (isThumbnailInclude, applicationNumber, productId)));
        }

        [HttpGet ("duplicateexists/{parametertype}/{parametervalue}")]
        public async Task<IActionResult> DuplicateExists (UniqueParameterTypes parametertype, string parametervalue) {
            try {
                return await ExecuteAsync (async () => {
                    return Ok (await Service.CheckIfDuplicateExists (parametertype, parametervalue));
                });
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        [HttpGet ("/expired/all")]
        public async Task<IActionResult> GetAllExpiredApplications () {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetAllExpiredApplications ());
            });
        }

        // [HttpPost("getapplicationsbyentityids/all")]
        // public async Task<IActionResult> GetApplicationsByEntityIds([FromBody]FilterApplication entityIds)
        // {
        //     return await ExecuteAsync(async () =>
        //     {
        //         return Ok(await Service.GetApplicationsByEntityIds(entityIds));
        //     });
        // }

        [HttpPost ("/expired/all/excludedStatuses")]
        public async Task<IActionResult> GetAllExpiredApplications ([FromBody] List<string> excludedStatuses) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetAllExpiredApplications (excludedStatuses));
            });
        }

        [HttpPost ("getapplicationsbyentityids/all")]
        public async Task<IActionResult> GetApplicationsByEntityIds ([FromBody] FilterApplication entityIds) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetApplicationsByEntityIds (entityIds));
            });
        }

        #region Search Application

        [HttpPost ("searchapplications/{condition?}")]
        public IActionResult SearchApplications ([FromBody] ApplicationSearchParams searchParams, string condition = null) {
            try {
                return Execute (() => {
                    return Ok (Service.SearchApplications (searchParams, condition));
                });
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        #endregion Search Application

        #region assignment filters

        [HttpGet ("/unassigned/all")]
        public Task<IActionResult> GetAllUnassigned () {
            return ExecuteAsync (async () => Ok (await Service.GetAllUnassignedByUser ()));
        }

        [HttpGet ("/mine")]
        public Task<IActionResult> GetAllAssignedApplications () {
            return ExecuteAsync (async () => Ok (await Service.GetAllAssignedApplications ()));
        }

        #endregion assignment filters

        #region Export Application

        [HttpGet ("export/all/{templatename}")]
        public IActionResult ExportAll (string templatename) {
            var result = Service.ExportAll (templatename);
            return new FileActionResult (result.FileDownloadName, result.FileContents, result.ContentType);
        }

        [HttpGet ("export/{templatename}/status/{*statuses}")]
        public async Task<IActionResult> ExportAllByStatus (string templatename, string statuses) {
            statuses = WebUtility.UrlDecode (statuses);
            var listStatus = statuses?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var result = await Service.ExportAllByStatus (templatename, listStatus);
            return new FileActionResult (result.FileDownloadName, result.FileContents, result.ContentType);
        }

        [HttpGet ("export/{templatename}/tag/{*tags}")]
        public async Task<IActionResult> ExportAllByTag (string templatename, string tags) {
            tags = WebUtility.UrlDecode (tags);
            var listTags = tags?.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var result = await Service.ExportAllByTag (templatename, listTags);
            return new FileActionResult (result.FileDownloadName, result.FileContents, result.ContentType);
        }

        #endregion Export Application

        #region Metric Collector Agrregate Quries

        [HttpGet ("groupapplicationstatus/all")]
        public async Task<IActionResult> GetPartnerIdApplicationCountByStatus () {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetApplicationCountByStatus ());
            });
        }

        [HttpGet ("groupapplicationstatus/{timeSpan}")]
        public async Task<IActionResult> GetPartnerIdApplicationCountByStatus (int timeSpan) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetApplicationCountByStatus (timeSpan));
            });
        }

        [HttpGet ("groupapplicationstatus/all/partner/{partnerId}")]
        public async Task<IActionResult> GetPartnerApplicationCountByStatus (string partnerId) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetPartnerApplicationCountByStatus (partnerId));
            });
        }

        [HttpGet ("groupapplicationstatus/partner/{partnerId}/{timeSpan}")]
        public async Task<IActionResult> GetPartnerApplicationCountByStatus (int timeSpan, string partnerId) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetPartnerApplicationCountByStatus (timeSpan, partnerId));
            });
        }

        #endregion Metric Collector Agrregate Quries

        [HttpGet ("aggregate/{applicationNumber}")]
        public async Task<IActionResult> GetByApplicationNumber (string applicationNumber) {
            return await ExecuteAsync (async () => {
                return Ok (await Service.GetByApplicationNumberWithTags (applicationNumber));
            });
        }
    }
}