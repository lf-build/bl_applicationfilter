﻿using System;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CapitalAlliance.Applications.Filters.Persistence;
using CreditExchange.StatusManagement.Client;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Funding.Client;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Partner.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.VerificationEngine.Client;

#if DOTNET2
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
#else

using LendFoundry.Foundation.Documentation;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

#endif

namespace CapitalAlliance.Applications.Filters.Api {
    public class Startup {
        public void ConfigureServices (IServiceCollection services) {
#if DOTNET2
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "application-filters"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CapitalAlliance.Applications.Filters.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

#else

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerDocumentation ();
#endif

            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();
            services.AddConfigurationService<Configuration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddConfigurationService<TaggingConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.TaggingEvents);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddApplicationService (Settings.Application.Host, Settings.Application.Port);
            services.AddApplicantService (Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddStatusManagementService (Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddDataAttributes (Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddVerificationEngineService (Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddProductRuleService (Settings.ProductRule.Host, Settings.ProductRule.Port);
            services.AddOfferEngineService (Settings.OfferEngine.Host, Settings.OfferEngine.Port);
            services.AddPartner (Settings.Partner.Host, Settings.Partner.Port);
            services.AddFundingService (Settings.businessFunding.Host, Settings.businessFunding.Port);
            // interface resolvers
            services.AddTransient (p => {
                var currentTokenReader = p.GetService<ITokenReader> ();
                var staticTokenReader = new StaticTokenReader (currentTokenReader.Read ());
                return p.GetService<IEventHubClientFactory> ().Create (staticTokenReader);
            });

            services.AddTransient (provider => provider.GetRequiredService<IConfigurationService<Configuration>> ().Get ());
            services.AddTransient (provider => provider.GetRequiredService<IConfigurationService<TaggingConfiguration>> ().Get ());
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IApplicationFilterService, ApplicationFilterService> ();
            services.AddTransient<IFilterViewRepository, FilterViewRepository> ();
            services.AddTransient<IFilterViewTagsRepository, FilterViewTagsRepository> ();
            services.AddTransient<IFilterViewTagsRepositoryFactory, FilterViewTagsRepositoryFactory> ();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory> ();
            services.AddTransient<IApplicationFilterListener, ApplicationFilterListener> ();
            services.AddTransient<IApplicationFilterServiceFactory, ApplicationFilterServiceFactory> ();
            services.AddAssignmentService (Settings.Assignment.Host, Settings.Assignment.Port);
            services.AddIdentityService (Settings.Identity.Host, Settings.Identity.Port);
            services.AddTransient<ICsvGenerator, CsvGenerator> ();
            services.AddEncryptionHandler ();
            // aspnet mvc related
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "ApplicationFilter Service");
            });
#else
            app.UseSwaggerDocumentation ();
#endif
            app.UseHealthCheck ();
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            //app.UseEventHub ();
            try {
                app.UseApplicationFilterListener ();
            } catch (Exception ex) {
                Console.WriteLine (ex.Message);
            }
            app.UseMvc ();
        }
    }
}