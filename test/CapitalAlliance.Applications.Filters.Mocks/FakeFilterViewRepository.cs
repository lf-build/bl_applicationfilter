﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Tests
{
    public class FakeFilterViewRepository : IFilterViewRepository
    {
        public ITenantTime TenantTime { get; set; }
        public List<IFilterView> FilterViews { get; } = new List<IFilterView>();

        public FakeFilterViewRepository(ITenantTime tenantTime, IEnumerable<IFilterView> filterViews) : this(tenantTime)
        {
            FilterViews.AddRange(filterViews);
            TenantTime = tenantTime;
        }

        public FakeFilterViewRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public void Add(IFilterView filterView)
        {
            if (!string.IsNullOrWhiteSpace(filterView.Id))
                throw new ArgumentException(nameof(filterView));
            FilterViews.Add(filterView);
        }

        public void AddOrUpdate(IFilterView filterView)
        {
            if (!string.IsNullOrWhiteSpace(filterView.Id))
                throw new ArgumentException(nameof(filterView));
            FilterViews.Add(filterView);
        }

        public Task<IEnumerable<IFilterView>> All(Expression<Func<IFilterView, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IFilterView, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<IFilterView> Get(string id)
        {
            return Task.FromResult(FilterViews.FirstOrDefault(filterView => filterView.Id.Equals(id)));
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return FilterViews;
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return OrderIt(FilterViews.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId));
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return Task.FromResult(FilterViews.FirstOrDefault(a => a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationNumber == applicationNumber));
        }

        public Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate)
        {
            return Task.FromResult(FilterViews.Where(p => p.ExpirationDate < todayDate));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(
                     FilterViews.Where(a => a.SourceType.ToLower() == sourceType.ToLower()
                                     && a.SourceId == sourceId
                                     && a.Applicant.ToLower().Contains(name.ToLower()))));
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return OrderIt(FilterViews.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId && statuses.Contains(x.StatusCode)));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(FilterViews.Where(x => statuses.Contains(x.StatusCode))));
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(from db in FilterViews.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags)
                        select db).Distinct()
            );
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<int>
            (
                FilterViews.Count(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode))
            );
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<int>
            (
                (from db in FilterViews.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                 from dbTags in db.Tags
                 where (from tag in tags select tag).Contains(dbTags)
                 select db).Distinct().Count()
            );
        }

        public double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses)
        {
            return FilterViews.Where(x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode))
                        .Sum(x => x.InitialOfferAmount);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth)
        {
            return Task.FromResult(FilterViews.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth
                ));
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear)
        {
            return Task.FromResult(FilterViews.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear
                ));
        }

        public Task<IFilterView> GetUsingUniqueAttributes(UniqueParameterTypes parameterType, string parameterValue)
        {
            Task<IFilterView> returnVal = null;
            switch(parameterType)
            {
                case UniqueParameterTypes.Mobile: returnVal = Task.FromResult(
                                                    FilterViews.FirstOrDefault(a => a.ApplicantPhone == parameterValue));
                    break;
                case UniqueParameterTypes.Email: returnVal = Task.FromResult(
                                                    FilterViews.FirstOrDefault(a => a.ApplicantPersonalEmail.ToLower() == parameterValue.ToLower()));
                    break;
                case UniqueParameterTypes.Pan: returnVal = Task.FromResult(
                                                    FilterViews.FirstOrDefault(a => a.ApplicantPanNumber.ToUpper() == parameterValue.ToUpper()));
                    break;
            }
            return returnVal;    
        }

        public void Remove(IFilterView item)
        {
            throw new NotImplementedException();
        }

        public void Update(IFilterView filterView)
        {
            //FilterViews.Remove(GetByApplicationNumber(filterView.SourceType, filterView.SourceId, filterView.ApplicationNumber));
            FilterViews.Add(filterView);
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(FilterViews.Where(x => statuses.Contains(x.StatusCode)))
            );
        }

        public Task<IFilterView> GetByApplicationNumber(string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public void AddTag(string applicationNumber, string tag)
        {
            throw new NotImplementedException();
        }

        public void RemoveTag(string applicationNumber, string tag)
        {
            throw new NotImplementedException();
        }
    }
}
