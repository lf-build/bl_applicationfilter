﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace CapitalAlliance.Applications.Filters.Client.Tests
{
    public class ApplicationFilterClientServiceTests
    {
        private ApplicationFilterClientService ApplicationFilterServiceClient { get; }

        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        public ApplicationFilterClientServiceTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationFilterServiceClient = new ApplicationFilterClientService(MockServiceClient.Object);
        }

        [Fact]
        public void Should_GetAll_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.Execute<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>

                        new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetAll();
            Assert.Equal("/all", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetAllBySource_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.Execute<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>

                        new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetAllBySource("Merchant", "SourceId");
            Assert.Equal("/{sourceType}/{sourceId}/all", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetBySourceAndStatus_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.Execute<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>

                        new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetBySourceAndStatus("Merchant", "SourceId", new string[] { "InProgress" });
            Assert.Equal("/{sourceType}/{sourceId}/status/{*statuses}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Should_GetTotalSubmittedForCurrentMonth_ReturnSubmittedCountForMonth()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<int>(It.IsAny<IRestRequest>()))
                .ReturnsAsync(1)
                .Callback<IRestRequest>(r => Request = r);

            var result = await ApplicationFilterServiceClient.GetTotalSubmittedForCurrentMonth("Merchant", "SourceId");
            Assert.Equal("/{sourceType}/{sourceId}/metrics/submit/month", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Should_GetTotalSubmittedForCurrentYear_ReturnSubmittedCountForYear()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<int>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(1);

            var result = await ApplicationFilterServiceClient.GetTotalSubmittedForCurrentYear("Merchant", "SourceId");
            Assert.Equal("/{sourceType}/{sourceId}/metrics/submit/year", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetByName_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetByName("Merchant", "SourceId", "1234");
            Assert.Equal("/{sourceType}/{sourceId}/search/{name}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetByStatus_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetByStatus("Merchant", "SourceId", new string[] { "InProgress" });
            Assert.Equal("/{sourceType}/{sourceId}/status/{*statuses}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetCountByStatus_ReturnCount()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<int>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(1);

            var result = ApplicationFilterServiceClient.GetCountByStatus("Merchant", "SourceId", new string[] { "InProgress" });
            Assert.Equal("/{sourceType}/{sourceId}/count/status/{*statuses}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetByTag_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<FilterView>()
                        {
                            new FilterView
                            {
                                Id = "123456"
                            }
                        }
                );

            var result = ApplicationFilterServiceClient.GetByTag("Merchant", "SourceId", new string[] { "Tag1" });
            Assert.Equal("/{sourceType}/{sourceId}/tag/{*tags}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetCountByTag_ReturnCount()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<int>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(1);

            var result = ApplicationFilterServiceClient.GetCountByTag("Merchant", "SourceId", new string[] { "Tag1" });
            Assert.Equal("/{sourceType}/{sourceId}/count/tag/{*tags}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetTotalAmountApprovedBySource_ReturnAmountApproved()
        {
            MockServiceClient.Setup(s => s.Execute<double>(It.IsAny<IRestRequest>()))
                .Returns(100).Callback<IRestRequest>(r => Request = r);

            var result = ApplicationFilterServiceClient.GetTotalAmountApprovedBySource("Merchant", "SourceId");
            Assert.Equal("/{sourceType}/{sourceId}/metrics/total-approved-amount", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetByApplicationNumber_ReturnFilterView()
         {
            MockServiceClient.Setup(s => s.ExecuteAsync<FilterView>(It.IsAny<IRestRequest>()))
                 .ReturnsAsync(new FilterView
                 {
                     Id = "123456",
                     ApplicationNumber = "1234"
                 }
                ).Callback<IRestRequest>(r => Request = r);

            var result = ApplicationFilterServiceClient.GetByApplicationNumber("Merchant", "SourceId", "1234");
            Assert.Equal("/{sourceType}/{sourceId}/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Should_GetAllExpiredApplications_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).ReturnsAsync(new List<FilterView>());

            ApplicationFilterServiceClient.GetAllExpiredApplications().Wait();
            Assert.Equal("/expired/all", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public void Should_GetAllByStatus_ReturnFilterViews()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r).ReturnsAsync(new List<FilterView>());

            var result = ApplicationFilterServiceClient.GetAllByStatus(new string[] { "InProgress" });
            Assert.Equal("status/{*statuses}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        ////bool CheckIfDuplicateExists
        //[Fact]
        //public void Should_CheckIfDuplicateExists_ReturnFilterViews()
        //{
        //    MockServiceClient.Setup(s => s.Execute<int>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(() => 1);

        //    var result = ApplicationFilterServiceClient.CheckIfDuplicateExists("BPZYPP0010", "9876543210", "abc@gmail.com", "abc", "d", "efg");
        //    Assert.Equal("/{sourceType}/{sourceId}/status/{*statuses}", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //    Assert.NotNull(result);
        //}
    }
}
