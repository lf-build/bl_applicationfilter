﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace CapitalAlliance.Applications.Filters.Tests
{
    public class ApplicationFilterServiceTests
    {
        private const string DummyApplicantId = "123";

        private const string DummyApplicationNumber = "1234";

        private static IApplicationFilterService GetApplicationFilterService()
        {
            return new ApplicationFilterService(GetFakeApplicationFilterViewRepository(GetFilterViewObject()), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), GetConfiguration(), GetTaggingConfiguration(), Mock.Of<IDecisionEngineService>(), Mock.Of<IEntityStatusService>());
        }

        private static IApplicationFilterService GetApplicationFilterService(Mock<ITenantTime> tenantTime)
        {
            return new ApplicationFilterService(GetFakeApplicationFilterViewRepository(GetFilterViewObject()), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), tenantTime.Object, GetConfiguration(), GetTaggingConfiguration(), Mock.Of<IDecisionEngineService>(), Mock.Of<IEntityStatusService>());
        }

        private static FakeFilterViewRepository GetFakeApplicationFilterViewRepository(List<IFilterView> filterViews = null)
        {
            return new FakeFilterViewRepository(new UtcTenantTime(), filterViews ?? new List<IFilterView>());
        }

        private static List<IFilterView> GetFilterViewObject()
        {
            return new List<IFilterView>
            {
                new FilterView
                    {
                        Id = "12345",
                        BusinessApplicantName = DummyApplicantId,
                        RequestedAmount = 100.00,

                        ApplicantAddressCity = "",
                        ApplicantAddressCountry = "",
                        ApplicantAddressIsDefault = true,
                        ApplicantAddressLine1 = "",
                        ApplicantAddressLine2 = "",
                        ApplicantAddressLine3 = "",
                        ApplicantAddressLine4 = "",

                        ApplicantFirstName = "abc",
                        ApplicantLastName = "efg",
                        ApplicantMiddleName = "d",

                        ApplicantPersonalEmail = "abc@gmail.com",
                        ApplicantPhone = "9876543210",
                        ApplicantWorkEmail = "",
                        ApplicationId = "1234",
                        ApplicationNumber = DummyApplicationNumber,
                        ExpirationDate = DateTime.Now,
                        FinalOfferAmount = 100.00,
                        FinalOfferInstallmentAmount = 100.00,
                        FinalOfferInterestRate = 12.50,
                        InitialOfferAmount = 100.00,
                        InitialOfferInterestRate = 10.25,
                        LastProgressDate = DateTime.Now,
                        RequestedTermType = "",
                        RequestedTermValue = 150.00,
                        SourceId = "SourceId",
                        SourceType = "Merchant",
                        StatusCode = "InProgress",
                        StatusDate = DateTime.Now,
                        StatusName = "",
                        Submitted = DateTime.Now.AddDays(10),
                        Tags = new List<string> { "Tag1" },
                        TenantId = "Tenant1"
                    }
            };
        }

        private static Configuration GetConfiguration()
        {
            return new Configuration
            {
                ApprovedStatuses = new string[] { "" },
                Events = new[] {
                    new EventMapping
                    {
                        ApplicationNumber = "",
                        Name = ""
                    }
                },
                Tags = new Dictionary<string, Rule>()
            };
        }

        private static TaggingConfiguration GetTaggingConfiguration()
        {
            return new TaggingConfiguration
            {
                Events = new List<TaggingEvent>()
            };
        }

        #region GetAll

        [Fact]
        public void Should_GetAll_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetAll());
        }

        #endregion GetAll

        #region GetAllBySource

        [Fact]
        public void Should_GetAllBySource_WithSourceTypeNull_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<InvalidArgumentException>(() => service.GetAllBySource(null, null));
        }

        [Fact]
        public void Should_GetAllBySource_WithSourceIdNull_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<InvalidArgumentException>(() => service.GetAllBySource("Merchant", null));
        }

        [Fact]
        public void Should_GetAllBySource_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetAllBySource("Merchant", "SourceId"));
        }

        #endregion GetAllBySource

        #region GetBySourceAndStatus

        [Fact]
        public void Should_GetBySourceAndStatus_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<InvalidArgumentException>(() => service.GetBySourceAndStatus(null, null, null));
        }

        [Fact]
        public void Should_GetBySourceAndStatus_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<InvalidArgumentException>(() => service.GetBySourceAndStatus("Merchant", null, null));
        }

        [Fact]
        public void Should_GetBySourceAndStatus_WithNullStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<ArgumentException>(() => service.GetBySourceAndStatus("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetBySourceAndStatus_WithZeroStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<ArgumentException>(() => service.GetBySourceAndStatus("Merchant", "SourceId", new List<string>()));
        }

        [Fact]
        public void Should_GetBySourceAndStatus_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetBySourceAndStatus("Merchant", "SourceId", new List<string> { "Status" }));
        }

        #endregion GetBySourceAndStatus

        #region GetTotalSubmittedForCurrentMonth

        [Fact]
        public void Should_GetTotalSubmittedForCurrentMonth_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetTotalSubmittedForCurrentMonth(null, null));
        }

        [Fact]
        public void Should_GetTotalSubmittedForCurrentMonth_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetTotalSubmittedForCurrentMonth("Merchant", null));
        }

        [Fact]
        public void Should_GetTotalSubmittedForCurrentMonth_Success()
        {
            var mockTenantTime = new Mock<ITenantTime>();
            mockTenantTime.Setup(t => t.Now).Returns(DateTime.Now);

            var service = GetApplicationFilterService(mockTenantTime);
            Assert.NotNull(service.GetTotalSubmittedForCurrentMonth("Merchant", "SourceId"));
        }

        #endregion GetTotalSubmittedForCurrentMonth

        #region GetTotalSubmittedForCurrentYear

        [Fact]
        public void Should_GetTotalSubmittedForCurrentYear_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetTotalSubmittedForCurrentYear(null, null));
        }

        [Fact]
        public void Should_GetTotalSubmittedForCurrentYear_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetTotalSubmittedForCurrentYear("Merchant", null));
        }

        [Fact]
        public void Should_GetTotalSubmittedForCurrentYear_Success()
        {
            var mockTenantTime = new Mock<ITenantTime>();
            mockTenantTime.Setup(t => t.Now).Returns(DateTime.Now);

            var service = GetApplicationFilterService(mockTenantTime);

            Assert.NotNull(service.GetTotalSubmittedForCurrentYear("Merchant", "SourceId"));
        }

        #endregion GetTotalSubmittedForCurrentYear

        #region GetByName

        [Fact]
        public void Should_GetByName_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByName(null, null, null));
        }

        [Fact]
        public void Should_GetByName_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByName("Merchant", null, null));
        }

        [Fact]
        public void Should_GetByName_WithNullName_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByName("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetByName_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetByName("Merchant", "SourceId", "Name"));
        }

        #endregion GetByName

        #region GetByStatus

        [Fact]
        public void Should_GetByStatus_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByStatus(null, null, null));
        }

        [Fact]
        public void Should_GetByStatus_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByStatus("Merchant", null, null));
        }

        [Fact]
        public void Should_GetByStatus_WithNullStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByStatus("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetByStatus_WithZeroStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByStatus("Merchant", "SourceId", new List<string>()));
        }

        [Fact]
        public void Should_GetByStatus_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetByStatus("Merchant", "SourceId", new List<string> { "Status" }));
        }

        #endregion GetByStatus

        #region GetCountByStatus

        [Fact]
        public void Should_GetCountByStatus_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByStatus(null, null, null));
        }

        [Fact]
        public void Should_GetCountByStatus_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByStatus("Merchant", null, null));
        }

        [Fact]
        public void Should_GetCountByStatus_WithNullStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByStatus("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetCountByStatus_WithZeroStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByStatus("Merchant", "SourceId", new List<string>()));
        }

        [Fact]
        public void Should_GetCountByStatus_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetCountByStatus("Merchant", "SourceId", new List<string> { "Status" }));
        }

        #endregion GetCountByStatus

        #region GetByTag

        [Fact]
        public void Should_GetByTag_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByTag(null, null, null));
        }

        [Fact]
        public void Should_GetByTag_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByTag("Merchant", null, null));
        }

        [Fact]
        public void Should_GetByTag_WithNullTags_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByTag("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetByTag_WithZeroTags_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByTag("Merchant", "SourceId", new List<string>()));
        }

        [Fact]
        public void Should_GetByTag_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetByTag("Merchant", "SourceId", new List<string> { "Tag" }));
        }

        #endregion GetByTag

        #region GetCountByTag

        [Fact]
        public void Should_GetCountByTag_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByTag(null, null, null));
        }

        [Fact]
        public void Should_GetCountByTag_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByTag("Merchant", null, null));
        }

        [Fact]
        public void Should_GetCountByTag_WithNullTags_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByTag("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetCountByTag_WithZeroTags_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetCountByTag("Merchant", "SourceId", new List<string>()));
        }

        [Fact]
        public void Should_GetCountByTag_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetCountByTag("Merchant", "SourceId", new List<string> { "Tag" }));
        }

        #endregion GetCountByTag

        #region GetTotalAmountApprovedBySource

        [Fact]
        public void Should_GetTotalAmountApprovedBySource_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<ArgumentException>(() => service.GetTotalAmountApprovedBySource(null, null));
        }

        [Fact]
        public void Should_GetTotalAmountApprovedBySource_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.Throws<ArgumentException>(() => service.GetTotalAmountApprovedBySource("Merchant", null));
        }

        [Fact]
        public void Should_GetTotalAmountApprovedBySource_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetTotalAmountApprovedBySource("Merchant", "SourceId"));
        }

        #endregion GetTotalAmountApprovedBySource

        #region GetByApplicationNumber

        [Fact]
        public void Should_GetByApplicationNumber_WithNullSourceType_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByApplicationNumber(null, null, null));
        }

        [Fact]
        public void Should_GetByApplicationNumber_WithNullSourceId_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByApplicationNumber("Merchant", null, null));
        }

        [Fact]
        public void Should_GetByApplicationNumber_WithNullApplicationNumber_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByApplicationNumber("Merchant", "SourceId", null));
        }

        [Fact]
        public void Should_GetByApplicationNumber_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetByApplicationNumber("Merchant", "SourceId", DummyApplicationNumber));
        }

        #endregion GetByApplicationNumber

        #region CheckIfDuplicateExists

        [Fact]
        public void Should_CheckIfDuplicateExists_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.CheckIfDuplicateExists(UniqueParameterTypes.Mobile, "9008444844"));
        }

        #endregion CheckIfDuplicateExists

        #region GetAllExpiredApplications

        [Fact]
        public void Should_GetAllExpiredApplications_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetAllExpiredApplications());
        }

        #endregion GetAllExpiredApplications

        #region GetAllByStatus

        [Fact]
        public void Should_GetAllByStatus_WithNullStatus_ThrowsArgumentException()
        {
            var service = GetApplicationFilterService();
            Assert.ThrowsAsync<ArgumentException>(() => service.GetAllByStatus(null));
        }

        [Fact]
        public void Should_GetAllByStatus_Success()
        {
            var service = GetApplicationFilterService();
            Assert.NotNull(service.GetAllByStatus(new string[] { "InProgress" }));
        }

        #endregion GetAllByStatus
    }
}