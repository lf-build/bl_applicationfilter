﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application;
using LendFoundry.Business.Application.Client;
using Configuration = CapitalAlliance.Applications.Filters.Abstractions.Configurations.Configuration;
using CreditExchange.StatusManagement;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using CapitalAlliance.Applications.Filters.Abstractions.Services;

namespace CapitalAlliance.Applications.Filters.Tests
{
    public class ApplicationFilterListenerTests
    {
        protected Mock<IConfigurationServiceFactory<Configuration>> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory<Configuration>>();

        protected Mock<IConfigurationService<Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Configuration>>();

        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        protected Mock<IConfigurationServiceFactory> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected Mock<IFilterViewRepositoryFactory> FilterViewRepositoryFactory { get; } = new Mock<IFilterViewRepositoryFactory>();

        protected Mock<IApplicationServiceClientFactory> ApplicationServiceClientFactory { get; } = new Mock<IApplicationServiceClientFactory>();

        protected Mock<Application.IApplicationService> ApplicationService { get; } = new Mock<Application.IApplicationService>();

        protected Mock<IApplicantServiceClientFactory> ApplicantServiceClientFactory { get; } = new Mock<IApplicantServiceClientFactory>();

        protected Mock<IApplicantService> ApplicantService { get; } = new Mock<IApplicantService>();

        protected Mock<IOfferEngineClientServiceFactory> OfferEngineServiceFactory { get; } = new Mock<IOfferEngineClientServiceFactory>();

        protected Mock<IOfferEngineService> OfferEngineService { get; } = new Mock<IOfferEngineService>();

        protected Mock<IStatusManagementServiceFactory> StatusManagementServiceFactory { get; } = new Mock<IStatusManagementServiceFactory>();

        protected Mock<IEntityStatusService> StatusManagementService { get; } = new Mock<IEntityStatusService>();

        protected Mock<ITenantServiceFactory> TenantServiceFactory { get; } = new Mock<ITenantServiceFactory>();
        protected Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();
        private Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();
        private Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        private Mock<IOfferEngineClientServiceFactory> OfferServiceFactory { get; } = new Mock<IOfferEngineClientServiceFactory>();

        private void SetupLogger()
        {
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Info(It.IsAny<string>(), It.IsAny<object>()));
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                            .Returns(Logger.Object);
        }

        private void SetupConfiguration(Abstractions.Configurations.Configuration configuration)
        {
            ConfigurationService.Setup(x => x.Get())
               .Returns(configuration);

            ConfigurationFactory.Setup(
                    x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);
        }

        private void SetupTenantService(List<TenantInfo> tenants)
        {
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            TenantService.Setup(x => x.GetActiveTenants()).Returns(tenants);
            TenantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(TenantService.Object);
        }

        private static List<TenantInfo> GetDummyTenants()
        {
            return new List<TenantInfo>()
            {
                new TenantInfo()
                {
                    Id = "Tenant1",
                    Name = "Tenant1"
                },
                new TenantInfo()
                {
                    Id = "Tenant2",
                    Name = "Tenant2"
                },
            };
        }

        private static Abstractions.Configurations.Configuration GetConfiguration()
        {
            return new Abstractions.Configurations.Configuration
            {
                Events =
                    new[]
                    {
                        new Abstractions.Configurations.EventMapping {Name = "ApplicationStatusChanged", ApplicationNumber = "{Data.EntityId}"}
                    }
            };
        }

        [Fact]
        public async void ApplicationFilterTest()
        {
            var fakeEventHub = new FakeEventHub();
            try
            {
                SetupLogger();

                SetupConfiguration(GetConfiguration());

                SetupTenantService(GetDummyTenants());
                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                    .Returns(() => fakeEventHub);
                var DummyApplicationNumber = "123";
                var DummyApplicantId = "123";

                var filterViews = new List<IFilterView>
                {
                    new FilterView
                    {
                        Id = "12345",
                        Applicant = DummyApplicantId,
                        AmountRequested = 100.00,
                        ApplicantAadharNumber = "",
                        ApplicantAddressCity = "",
                        ApplicantAddressCountry = "",
                        ApplicantAddressIsDefault = true,
                        ApplicantAddressLine1 = "",
                        ApplicantAddressLine2 = "",
                        ApplicantAddressLine3 = "",
                        ApplicantAddressLine4 = "",
                        ApplicantEmployer = "",
                        ApplicantFirstName = "",
                        ApplicantLastName = "",
                        ApplicantMiddleName = "",
                        ApplicantPanNumber = "",
                        ApplicantPersonalEmail = "",
                        ApplicantPhone = "",
                        ApplicantWorkEmail = "",
                        ApplicationId = "1234",
                        ApplicationNumber = DummyApplicationNumber,
                        ExpirationDate = DateTime.Now,
                        FinalOfferAmount = 100.00,
                        FinalOfferInstallmentAmount = 100.00,
                        FinalOfferInterestRate = 12.50,
                        InitialOfferAmount = 100.00,
                        InitialOfferInterestRate = 10.25,
                        LastProgressDate = DateTime.Now,
                        RequestTermType = "",
                        RequestTermValue = 150.00,
                        SourceId = "SourceId",
                        SourceType = "Merchant",
                        StatusCode = "InProgress",
                        StatusDate = DateTime.Now,
                        StatusName = "",
                        Submitted = DateTime.Now,
                        Tags = new List<string> { "Tag1" },
                        TenantId = "Tenant1"
                    }
                };

                var fakeFilterViewRepository = new FakeFilterViewRepository(new UtcTenantTime(), filterViews);

                FilterViewRepositoryFactory.Setup(f => f.Create(It.IsAny<ITokenReader>())).Returns(fakeFilterViewRepository);
                ApplicationServiceClientFactory.Setup(a => a.Create(It.IsAny<ITokenReader>())).Returns(ApplicationService.Object);
                ApplicantServiceClientFactory.Setup(a => a.Create(It.IsAny<ITokenReader>())).Returns(ApplicantService.Object);
                OfferEngineServiceFactory.Setup(o => o.Create(It.IsAny<ITokenReader>())).Returns(OfferEngineService.Object);
                StatusManagementServiceFactory.Setup(s => s.Create(It.IsAny<ITokenReader>())).Returns(StatusManagementService.Object);

                ApplicationService.Setup(a => a.GetByApplicationNumber(It.IsAny<string>())).Returns(Task.FromResult<IApplication>(new Application.Application
                {
                    ApplicantId = DummyApplicantId,
                    ApplicationDate = new TimeBucket(DateTimeOffset.Now),
                    ApplicationNumber = DummyApplicationNumber,                   
                    EmploymentDetail = Mock.Of<IEmploymentDetail>(),
                    ExpiredDate = new TimeBucket(DateTimeOffset.Now),
                    ExpiryDate = new TimeBucket(DateTimeOffset.Now),
                    Id = "123",                    
                    OtherPurposeDescription = "",
                    PurposeOfLoan = "HomeBuying",
                    RequestedAmount = 1000000.00,
                    RequestedTermType = LoanFrequency.Monthly,
                    RequestedTermValue = 1000000.00,
                    ResidenceType = "Own",
                    SelfDeclareExpense = new SelfDeclareExpense(),
                    Source = new Source { SourceType = SourceType.Merchant, SourceReferenceId = "SourceId"},
                    TenantId = ""
                }));

                ApplicantService.Setup(a => a.Get(It.IsAny<string>())).Returns(Task.FromResult<IApplicant>(new Applicant.Applicant
                {
                    AadhaarNumber = "",
                    Addresses = new List<Applicant.IAddress>(),
                    BankInformation = new List<Applicant.IBankInformation>(),
                    DateOfBirth = DateTime.Now,
                    EmailAddresses = new List<Applicant.IEmailAddress>(),
                    EmploymentDetails = new List<Applicant.IEmploymentDetail>(),
                    FirstName = "",
                    Gender = new Applicant.Gender(),
                    Id = "123",
                    IncomeInformation = new List<Applicant.IIncomeInformation>(),
                    LastName = "",
                    MaritalStatus = Applicant.MaritalStatus.Single,
                    MiddleName = "",
                    PermanentAccountNumber = "",
                    PhoneNumbers = new List<Applicant.IPhoneNumber>(),
                    Salutation = "",
                    TenantId = "",
                    UserId = ""
                }));

                StatusManagementService.Setup(s => s.GetStatusByEntity(It.IsAny<string>(), It.IsAny<string>())).Returns(new StatusResponse() { Name = "InProgress"});

                var applicationListener = new ApplicationFilterListener(ConfigurationFactory.Object,
                                                                TokenHandler.Object, EventHubFactory.Object,
                                                                FilterViewRepositoryFactory.Object,
                                                                LoggerFactory.Object, TenantServiceFactory.Object, ApplicationServiceClientFactory.Object,
                                                                ApplicantServiceClientFactory.Object, StatusManagementServiceFactory.Object,
                                                                ConfigurationServiceFactory.Object, TenantTimeFactory.Object, OfferServiceFactory.Object);

                applicationListener.Start();

                await fakeEventHub.Publish("ApplicationStatusChanged", new { EntityId = DummyApplicationNumber, NewStatus = "InProgress" });

                var application = await fakeFilterViewRepository.GetByApplicationNumber("Merchant", "SourceId", DummyApplicationNumber);
                Assert.Equal("InProgress", application.StatusCode);
            }
            finally
            {
                fakeEventHub.Close();
            }
        }
    }
}
