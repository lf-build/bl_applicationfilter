FROM microsoft/dotnet:2.0.6-sdk-2.1.101-stretch AS build-env
ARG RESTORE
RUN : "${RESTORE:?Build argument needs to be set and non-empty.}"
WORKDIR /app

COPY ./src/ /app/

RUN find . -name "global.json"  -type f -delete && find . -name "NuGet.config"  -type f -delete && find . -name "*.xproj"  -type f -delete

WORKDIR /app/CapitalAlliance.Applications.Filters.Api
RUN $RESTORE && dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0.6
WORKDIR /app/
COPY --from=build-env /app/CapitalAlliance.Applications.Filters.Api/out .


ENTRYPOINT ["dotnet", "CapitalAlliance.Applications.Filters.Api.dll"]

